#ifndef COMMANAGER_H
#define COMMANAGER_H
#include <ros/ros.h>
#include <list>
#include <atomic>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <mavlink/v1.0/common/mavlink.h>
#include "std_msgs/String.h"
#include "geometry_msgs/Pose.h"
#include "comsocket.h"
#include "com/Mavlink.h"
#include <pluginlib/class_loader.h>
#include <com/MavrosMsgPlugin.h>

class ComManager
{
public:
  ComManager(ros::NodeHandle &nh);
  ~ComManager();
  void exec();
  void postData(mavlink_message_t &msg);

private:
  boost::asio::io_service m_io;
  std::unique_ptr<boost::asio::io_service::work> m_work;
  boost::asio::deadline_timer m_t;
  int m_timer_duration;
  ros::Subscriber m_sub;
  std::list<ComSocket*> SocketList;
  pluginlib::ClassLoader<mavros::MavrosPlugin> m_plugin_loader;
  std::vector<mavros::MavrosPlugin::Ptr> m_loaded_plugins;
  std::array<std::list<mavros::MavrosPlugin::message_handler>, 256> m_route_table;
  ros::NodeHandle m_nh;

  void rosSpinOnce(const boost::system::error_code &error);
  void msgCallback(const com::Mavlink::ConstPtr& rmsg);
  void endSpin();
  void processData(mavlink_message_t msg);
  void add_plugin(std::string &pl_name);
};

#endif // COMMANAGER_H
