#ifndef MSGBUF_H
#define MSGBUF_H
#include <mavlink/v1.0/common/mavlink.h>
struct MsgBuffer {
    //! Maximum buffer size with padding for CRC bytes (280 + padding)
    static constexpr ssize_t MAX_SIZE = MAVLINK_MAX_PACKET_LEN + 16;
    uint8_t data[MAX_SIZE];
    ssize_t len;
    ssize_t pos;

    MsgBuffer() :
        pos(0),
        len(0)
    { }

    /**
     * @brief Buffer constructor from mavlink_message_t
     */
    explicit MsgBuffer(mavlink_message_t *msg, uint8_t sysId, uint8_t compId,
                       uint8_t chan, uint8_t msg_len, uint8_t msg_key) :
        pos(0)
    {

        mavlink_finalize_message_chan(msg, sysId, compId, chan, msg_len, msg_key);
        len = mavlink_msg_to_send_buffer(data, msg);
        // paranoic check, it must be less than MAVLINK_MAX_PACKET_LEN
        assert(len < MAX_SIZE);
    }


    /**
     * @brief Buffer constructor for send_bytes()
     * @param[in] nbytes should be less than MAX_SIZE
     */
    MsgBuffer(const uint8_t *bytes, ssize_t nbytes) :
        pos(0),
        len(nbytes)
    {
        assert(0 < nbytes && nbytes < MAX_SIZE);
        memcpy(data, bytes, nbytes);
    }

    virtual ~MsgBuffer() {
        pos = 0;
        len = 0;
    }

    uint8_t *dpos() {
        return data + pos;
    }

    ssize_t nbytes() {
        return len - pos;
    }
};
#endif // MSGBUF_H
