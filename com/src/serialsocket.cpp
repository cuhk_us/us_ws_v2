#include "com/serialsocket.h"
#include <com/commanager.h>

SerialSocket::SerialSocket(uint8_t system_id, uint8_t component_id,
                           std::string device, unsigned baudrate, bool hwflow, ComManager* mgr):
    ComSocket(mgr),
    m_serial_dev(m_io),
    m_tx_in_progress(false),
    tx_q {},
    rx_buf {},
    m_system_id(system_id),
    m_component_id(component_id),
    m_device(device),
    m_baudrate(baudrate),
    m_hwflow(hwflow),
    m_sending(false)
{

}

void SerialSocket::setupSocket()
{
    using SPB = boost::asio::serial_port_base;
    try {
        m_serial_dev.open(m_device);

        // Set baudrate and 8N1 mode
        m_serial_dev.set_option(SPB::baud_rate(m_baudrate));
        m_serial_dev.set_option(SPB::character_size(8));
        m_serial_dev.set_option(SPB::parity(SPB::parity::none));
        m_serial_dev.set_option(SPB::stop_bits(SPB::stop_bits::one));

#if BOOST_ASIO_VERSION >= 101200 || !defined(__linux__)
        // Flow control setting in older versions of Boost.ASIO is broken, use workaround (below) for now.
        m_serial_dev.set_option(SPB::flow_control( (hwflow) ? SPB::flow_control::hardware : SPB::flow_control::none));
#elif BOOST_ASIO_VERSION < 101200 && defined(__linux__)
        // Workaround to set some options for the port manually. This is done in
        // Boost.ASIO, but until v1.12.0 (Boost 1.66) there was a bug which doesn't enable relevant
        // code. Fixed by commit: https://github.com/boostorg/asio/commit/619cea4356
        int fd = m_serial_dev.native_handle();
        termios tio;
        tcgetattr(fd, &tio);

        // Set hardware flow control settings
        if (m_hwflow) {
            tio.c_iflag &= ~(IXOFF | IXON);
            tio.c_cflag |= CRTSCTS;
        } else {
            tio.c_iflag &= ~(IXOFF | IXON);
            tio.c_cflag &= ~CRTSCTS;
        }

        // Set serial port to "raw" mode to prevent EOF exit.
        cfmakeraw(&tio);

        // Commit settings
        tcsetattr(fd, TCSANOW, &tio);
#endif
    }
    catch (boost::system::system_error &err) {
        throw DeviceError("serial", err);
    }


    m_io.post(std::bind(&SerialSocket::read_msg, this));
}

void SerialSocket::endSocket()
{
    printf("--------------------------------\n");
    m_serial_dev.cancel();
    m_serial_dev.close();
}

void SerialSocket::read_msg()
{
    m_serial_dev.async_read_some(
                boost::asio::buffer(rx_buf),
                [this] ( boost::system::error_code error, size_t bytes_transferred)
    { //Lambada function content
        if (error)
        {
            printf("Serial Socket Read Error: %s\n", error.message().c_str());
            close_in_worker_thread();
            return;
        }

        // Try decoding
        mavlink_message_t message;
        mavlink_status_t status;
        for (ssize_t i = 0; i < bytes_transferred; i++)
        {
            if (mavlink_parse_char(1, rx_buf[i], &message, &status))
            {
                //Post the data
                m_mgr->postData(message);
            }
        }
        read_msg();
    });
}

void SerialSocket::send_msg(const mavlink_message_t &msg)
{
    //std::cout<<"send at: "<<std::this_thread::get_id()<<std::endl;
    m_io.post(boost::bind(&SerialSocket::send_in_worker_thread, this, msg));
}

void SerialSocket::send_in_worker_thread(mavlink_message_t msg)
{
    //std::cout<<"send in thread at: "<<std::this_thread::get_id()<<std::endl;
    tx_q.emplace_back(&msg,255,50,1,msg.len,messageKeys[msg.msgid]);
    if (m_sending)
        return;
    else
        do_write();
}

void SerialSocket::do_write()
{
    //std::cout<<"do write at: "<<std::this_thread::get_id()<<std::endl;
    m_sending = true;
    auto &buf_ref = tx_q.front();
    m_serial_dev.async_write_some(
                boost::asio::buffer(buf_ref.dpos(), buf_ref.nbytes()),
                [this, &buf_ref]( boost::system::error_code error, size_t bytes_transferred)
    {
        assert(bytes_transferred <= buf_ref.len);
        if (error)
        {
            printf("Serial Socket Write Error: %s\n", error.message().c_str());
            close_in_worker_thread();
            return;
        }

        if(tx_q.empty())
        {
            m_sending = false;
            return;
        }
        buf_ref.pos += bytes_transferred;
        if(buf_ref.nbytes()==0)
        {
            tx_q.pop_front();
        }

        if(!tx_q.empty())
            do_write();
        else
            m_sending = false;
    });
}
