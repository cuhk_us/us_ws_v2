#include "com/udpsocket.h"
#include <com/commanager.h>

UdpSocket::UdpSocket(uint8_t system_id, uint8_t component_id,
                     std::string bind_host, unsigned short bind_port,
                     std::string remote_host, unsigned short remote_port,ComManager* mgr):
    ComSocket(mgr),
    m_socket(m_io),
    tx_q {},
    rx_buf {},
    m_system_id(system_id),
    m_component_id(component_id),
    m_sending(false)
{
    // set the local and remote endpoint
    m_bind_ep.address(boost::asio::ip::address::from_string(bind_host));
    m_bind_ep.port(bind_port);
    m_remote_ep.address(boost::asio::ip::address::from_string(remote_host));
    m_remote_ep.port(remote_port);
}

void UdpSocket::setupSocket()
{
    using udps = boost::asio::ip::udp::socket;
    try {
        m_socket.open(boost::asio::ip::udp::v4());
        m_socket.bind(m_bind_ep);

        // set buffer opt. size from QGC
        m_socket.set_option(udps::reuse_address(true));
        m_socket.set_option(udps::send_buffer_size(256*1024));
        m_socket.set_option(udps::receive_buffer_size(512*1024));
    }
    catch (boost::system::system_error &err) {
        throw DeviceError("udp", err);
    }

    // give some work to io_service before start
    m_io.post(std::bind(&UdpSocket::read_msg, this));
}

void UdpSocket::endSocket()
{
    printf("--------------------------------\n");
    m_socket.cancel();
    m_socket.close();
}

void UdpSocket::read_msg()
{
    m_socket.async_receive(
                boost::asio::buffer(rx_buf),
                [this] ( boost::system::error_code error, size_t bytes_transferred)
    { //Lambada function content
        if (error)
        {
            printf("UDP Socket Read Error: %s\n", error.message().c_str());
            close_in_worker_thread();
            return;
        }

        // Try decoding
        mavlink_message_t message;
        mavlink_status_t status;
        for (ssize_t i = 0; i < bytes_transferred; i++)
        {
            if (mavlink_parse_char(1, rx_buf[i], &message, &status))
            {
                //Post the data
                m_mgr->postData(message);
            }
        }
        read_msg();
    });
}

void UdpSocket::send_msg(const mavlink_message_t &msg)
{
    m_io.post(boost::bind(&UdpSocket::send_in_worker_thread, this, msg));
}

void UdpSocket::send_in_worker_thread(mavlink_message_t msg)
{
    tx_q.emplace_back(&msg,255,50,1,msg.len,messageKeys[msg.msgid]);
    if (m_sending)
        return;
    else
        do_write();
}

void UdpSocket::do_write()
{
    m_sending = true;
    auto &buf_ref = tx_q.front();
    m_socket.async_send_to(
                boost::asio::buffer(buf_ref.dpos(), buf_ref.nbytes()),
                m_remote_ep,
                [this, &buf_ref]( boost::system::error_code error, size_t bytes_transferred)
    {
        assert(bytes_transferred <= buf_ref.len);
        if (error && error != boost::asio::error::network_unreachable)
        {
            printf("Serial Socket Write Error: %s\n", error.message().c_str());
            close_in_worker_thread();
            return;
        }

        if(tx_q.empty())
        {
            m_sending = false;
            return;
        }
        buf_ref.pos += bytes_transferred;
        if(buf_ref.nbytes()==0)
        {
            tx_q.pop_front();
        }

        if(!tx_q.empty())
            do_write();
        else
            m_sending = false;
    });
}
