#include <com/MavrosMsgPlugin.h>
#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/Point.h>
#include <gazebo_msgs/GetModelState.h>
namespace mavros {

class StatusPlugin : public MavrosPlugin
{
public:
    StatusPlugin()
    {
        ROS_INFO_NAMED("status", "status constructor");
    }

    ~StatusPlugin()
    {
        ROS_INFO_NAMED("status", "status destructor");
    }

    void initialize(ComSocket *socket, ros::NodeHandle &nh)
    {
        ROS_INFO_NAMED("status", "initialize");
        m_forward_socket = socket;

        m_pub = nh.advertise<geometry_msgs::Pose>("/pose",10);
    }

    std::string getName()
    {
        return "status";
    }

    const message_map get_msg_handlers() {
        return {
            MESSAGE_HANDLER(MAVLINK_MSG_ID_HEARTBEAT, &StatusPlugin::heartBeat),
            MESSAGE_HANDLER(MAVLINK_MSG_ID_LOCAL_POSITION_NED, &StatusPlugin::localPos)
        };
    }

private:
    void heartBeat(const mavlink_message_t *msg, uint8_t sysid, uint8_t compid)
    {
        ROS_INFO_NAMED("status", "Heartbeat2");
    }

    void localPos(const mavlink_message_t *msg, uint8_t sysid, uint8_t compid)
    {
        mavlink_local_position_ned_t pos;
        mavlink_msg_local_position_ned_decode(msg, &pos);
        geometry_msgs::Pose localPos;
        localPos.position.x=pos.x;
        localPos.position.y=pos.y;
        localPos.position.z=pos.z;
        m_pub.publish(localPos);
    }

private:
      ros::Publisher m_pub;
};

} // namespace mavros

PLUGINLIB_EXPORT_CLASS(mavros::StatusPlugin, mavros::MavrosPlugin)


