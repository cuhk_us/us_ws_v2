#include <usr_console/ConsoleMsgPlugin.h>
#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PoseStamped.h>
#include <gazebo_msgs/GetModelState.h>
#include <geometry_msgs/TransformStamped.h>
#include <common_msgs/state.h>
#include <sstream>
#include <string>
#include <fstream>
#include <tf/tf.h>
#include <std_srvs/Trigger.h> 
namespace consoleplugin {
class DdronePlugin : public ConsoleMsgPlugin
{
public:
    DdronePlugin()
    {
        ROS_INFO_NAMED("DdronePlugin", "DdronePlugin constructor");
    }

    ~DdronePlugin()
    {

    }

    void initialize(ros::NodeHandle &nh)
    {
        ROS_INFO_NAMED("DdronePlugin", "initialize");
        m_nh = ros::NodeHandle("~");
        m_pos_cmd_pub = m_nh.advertise<geometry_msgs::Point>("/dummy/pos_cmd",1);
        m_current_state_sub = m_nh.subscribe("/rt_ref_gen/current_state", 1,
                                             &DdronePlugin::currentStateCallback, this);
        m_uav_pose = ros::topic::waitForMessage<geometry_msgs::PoseStamped>("/mavros/position/local", m_nh);
        mission_started = false;
        m_jump = false;
        m_target_timer = m_nh.createTimer(ros::Duration(0.2),&DdronePlugin::checkReached,this,false,false);

        m_nh.param<std::string>("home_name",m_home_name,"/home/nvidia");
        ROS_WARN_STREAM("home_name = " << m_home_name);
        client = m_nh.serviceClient<std_srvs::Trigger>("/mavros/engine0");
    }

    std::string getName()
    {
        return "DdronePlugin";
    }

    const message_map get_msg_handlers() {
        return {
            MESSAGE_HANDLER("waypoints", &DdronePlugin::startMission),
              MESSAGE_HANDLER("next_wp", &DdronePlugin::jumpWp),
              MESSAGE_HANDLER("engine0", &DdronePlugin::engine0)
        };
    }

private:
    void startMission(const std::vector<double> &parameters)
    {
        //Read in the file for waypoints
        _wp_list.clear();
        std::ifstream infile(m_home_name);
        std::string line;
        tf::Quaternion q(
        m_uav_pose->pose.orientation.x,
        m_uav_pose->pose.orientation.y,
        m_uav_pose->pose.orientation.z,
        m_uav_pose->pose.orientation.w);
        tf::Matrix3x3 m(q);
        double roll, pitch, yaw, cos_y, sin_y;
        m.getRPY(roll, pitch, yaw);
        cos_y = cos(yaw);
        sin_y = sin(yaw);
        while (std::getline(infile, line))
        {
            std::istringstream iss(line);
            double _1, _2, _3;
            if (!(iss >> _1 >> _2 >> _3)) { break; } // error
            geometry_msgs::Point p;
            p.x = _1 * cos_y - _2 * sin_y;
            p.y = _1 * sin_y + _2 * cos_y;
            p.z = _3;
            _wp_list.push_back(p);
            std::cout<<p.x<<" "<<p.y<<" "<<p.z<<std::endl;
        }
        _wp_id = 0;
        mission_started = true;
        m_target_timer.start();

        // publish the first target
        if (_wp_list.size()>0)
            m_pos_cmd_pub.publish(_wp_list[_wp_id]);
    }

    void jumpWp(const std::vector<double> &parameters)
    {
        m_jump = true;
    }

    void engine0(const std::vector<double> &parameters)
    {
        std_srvs::Trigger srv;
        if (client.call(srv))
        {
            if(srv.response.success)
            {
                ROS_INFO("Engine0 is activated");
            }
        }
        else
        {
            ROS_ERROR("Failed to activate engine0");
        }
    }

    void currentStateCallback(const common_msgs::state::ConstPtr& msg)
    {
        // update the current uav position
        _curr.x = msg->pos.x;
        _curr.y = msg->pos.y;
        _curr.z = msg->pos.z;
    }

    void checkReached(const ros::TimerEvent&)
    {
        if (!mission_started) return;
        // calculate the distance between the current position to the wp
        geometry_msgs::Point dist;
        dist.x = _curr.x - _wp_list[_wp_id].x;
        dist.y = _curr.y - _wp_list[_wp_id].y;
        dist.z = _curr.z - _wp_list[_wp_id].z;

        // if dist is small enough
        if (dist.x*dist.x + dist.y*dist.y < 1.0 || m_jump)
        {
            if (_wp_id < _wp_list.size()-1)
            {
                _wp_id++;
                m_pos_cmd_pub.publish(_wp_list[_wp_id]);
                std::cout<<"Goto wp: "<<_wp_id<<std::endl;
            }
            m_jump = false;
        }
    }

private:
    ros::NodeHandle m_nh;
    ros::Publisher  m_pos_cmd_pub;
    ros::Subscriber m_current_state_sub;
    std::vector<geometry_msgs::Point> _wp_list;
    geometry_msgs::Point _curr;
    geometry_msgs::PoseStampedConstPtr m_uav_pose;
    bool mission_started;
    bool m_jump;
    ros::Timer m_target_timer;
    int _wp_id;
    std::string m_home_name;
    ros::ServiceClient client;
};

} // namespace consoleplugin

PLUGINLIB_EXPORT_CLASS(consoleplugin::DdronePlugin, consoleplugin::ConsoleMsgPlugin)

