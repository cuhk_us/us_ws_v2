#include "usr_console/ConsoleMgr.h"
#include <boost/thread/thread.hpp>
#include <std_srvs/Empty.h>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/split.hpp>
#include <stdexcept>      // std::out_of_range

ConsoleMgr::ConsoleMgr(ros::NodeHandle &nh):
    m_nh(nh),
    m_work(new boost::asio::io_service::work(m_io)),
    m_t(m_io),
    m_timer_duration(1),
    m_plugin_loader("usr_console","consoleplugin::ConsoleMsgPlugin")
{
    m_input = new AsyncIn(this);
    for (auto &name : m_plugin_loader.getDeclaredClasses())
        add_plugin(name);
}

ConsoleMgr::~ConsoleMgr()
{
    m_input->stop();
    delete m_input;
}

void ConsoleMgr::exec()
{
    m_t.expires_from_now(boost::posix_time::milliseconds(m_timer_duration));
    m_t.async_wait(boost::bind(&ConsoleMgr::rosSpinOnce, this,
                               boost::asio::placeholders::error));
    m_input->start();
    m_io.run();
}

void ConsoleMgr::rosSpinOnce(const boost::system::error_code &error)
{
    if (error)
    {
        printf("Ros Spin Error: %s\n", error.message().c_str());
        endSpin();
        return;
    }
    if (ros::ok())
    {
        ros::spinOnce();
        m_t.expires_at(m_t.expires_at() + boost::posix_time::milliseconds(m_timer_duration));
        m_t.async_wait(boost::bind(&ConsoleMgr::rosSpinOnce, this,
                                   boost::asio::placeholders::error));
    }
    else
    {
        endSpin();
    }
}

void ConsoleMgr::endSpin()
{
    m_work.reset();
    m_io.stop();
}

void ConsoleMgr::handleStdin(const std::string & input)
{
    m_io.post(boost::bind(&ConsoleMgr::stdinCallback,this,input));
}

void ConsoleMgr::stdinCallback(const std::string msg)
{
    //first try split the command
    if (!readCommand(msg,m_cmd,m_parameters))
        return;

    try
    {
       consoleplugin::ConsoleMsgPlugin::message_handler hf = m_route_table.at(m_cmd);
       hf(m_parameters);
    }
    catch (const std::out_of_range& oor)
    {
        std::cout << "The command does not exist: " << oor.what() << '\n';
    }
}

bool ConsoleMgr::readCommand(const std::string &input_str, std::string &cmd, std::vector<double> &parameters)
{
    // seperate cmd & parameters
    std::vector<std::string> tokens;
    boost::split(tokens, input_str, boost::is_any_of(":("));
    if(2==tokens.size())
    {
        // get cmd
        cmd = trim(tokens.at(0));

        // get parameters
        parameters.clear();
        std::stringstream ss(trim(tokens.at(1)));
        double x;
        while(ss >> x)
        {
            parameters.push_back(x);
        }
        return true;
    }
    else if(1==tokens.size())
    {
        // get cmd
        cmd = trim(tokens.at(0));

        // clear parameters
        parameters.clear();
        return true;
    }
    else
    {
        ROS_INFO("Command format error. Please use format \"cmd: para1 para2 ...\"");
        return false;
    }
}

std::string ConsoleMgr::trim(const std::string& str, const std::string& whitespace)
{
    const size_t strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos)
        return ""; // no content

    const size_t strEnd = str.find_last_not_of(whitespace);
    const size_t strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}

void ConsoleMgr::add_plugin(std::string &pl_name)
{
    boost::shared_ptr<consoleplugin::ConsoleMsgPlugin> plugin;

    try
    {
        plugin = m_plugin_loader.createInstance(pl_name);
        plugin->initialize(m_nh);
        m_loaded_plugins.push_back(plugin);
        std::string repr_name = plugin->getName();

        ROS_INFO_STREAM("Plugin " << repr_name <<" [alias " << pl_name << "] loaded and initialized");

        for (auto &pair : plugin->get_msg_handlers())
        {
            ROS_DEBUG("Route msgid %d to %s", pair.first, repr_name.c_str());
            m_route_table[pair.first] = pair.second;
        }
    }
    catch (pluginlib::PluginlibException &ex)
    {
        ROS_ERROR_STREAM("Plugin [alias " << pl_name << "] load exception: " << ex.what());
    }
}
