#include "usr_console/AsyncIn.h"
#include "usr_console/ConsoleMgr.h"
#include <boost/thread/thread.hpp>
AsyncIn::AsyncIn(ConsoleMgr *mgr):
    m_work(new boost::asio::io_service::work(m_io)),
    m_mgr(mgr)
{

}

AsyncIn::~AsyncIn()
{
    std::cout<<"AsyncIn destructed."<<std::endl;
}

void AsyncIn::start()
{
    m_io.post(boost::bind(&AsyncIn::read_msg,this));
    m_thread = std::thread([this] () {
        m_io.run();
    });
}

void AsyncIn::read_msg()
{
    std::string cmd;
    std::getline (std::cin,cmd);
    m_mgr->handleStdin(cmd);
    m_io.post(boost::bind(&AsyncIn::read_msg,this));
}

void AsyncIn::stop()
{
    pthread_cancel(m_thread.native_handle());
    if (m_thread.joinable())
    {
        m_thread.join();
    }
}
