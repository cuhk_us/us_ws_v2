#ifndef ASYNCIN_H
#define ASYNCIN_H
#include <ros/ros.h>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <thread>
#include <mutex>

class ConsoleMgr;
class AsyncIn
{
public:
    AsyncIn(ConsoleMgr*);
    ~AsyncIn();
    void start();
    void stop();

private:
    boost::asio::io_service m_io;
    std::unique_ptr<boost::asio::io_service::work> m_work;
    std::thread m_thread;
    ConsoleMgr* m_mgr;

private:
    void read_msg();
};

#endif // ASYNCIN_H
