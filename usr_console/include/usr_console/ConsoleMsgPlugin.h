#ifndef CONSOLEMSGPLUGIN_H
#define CONSOLEMSGPLUGIN_H
#include <map>
#include <string.h>
#include <vector>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <ros/ros.h>

namespace consoleplugin
{
#define MESSAGE_HANDLER(_message, _class_method_ptr)	\
{ _message, boost::bind(_class_method_ptr, this, _1) }

class ConsoleMsgPlugin
{
public:
    typedef boost::function<void(const std::vector<double> &parameters)> message_handler;
    typedef std::map<std::string, message_handler> message_map;
    // pluginlib return boost::shared_ptr
    typedef boost::shared_ptr<ConsoleMsgPlugin> Ptr;
    typedef boost::shared_ptr<ConsoleMsgPlugin const> ConstPtr;

public:
    virtual void initialize(ros::NodeHandle &nh) = 0;
    virtual const message_map get_msg_handlers() = 0;
    virtual std::string getName() = 0;
    virtual ~ConsoleMsgPlugin(){}

protected:
    ConsoleMsgPlugin(){}

private:
    ConsoleMsgPlugin(const ConsoleMsgPlugin&) = delete;
};
}

#endif // CONSOLEMSGPLUGIN_H
