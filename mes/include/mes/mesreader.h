#ifndef MESREADER_H
#define MESREADER_H
#include <com/Mavlink.h>
#include <mavlink/v1.0/common/mavlink.h>
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include "com/utils.h"

#define USEVICON
class MesReader
{
public:
    MesReader(ros::NodeHandle & nh);
    ~MesReader();

private:
    double x,y,z,a,b,c,d;
    ros::Subscriber m_sub;
    ros::Publisher m_pub;
    ros::Timer m_timer;
    void timerCallback(const ros::TimerEvent&);
#ifdef USEVICON
    void msgCallback(const geometry_msgs::TransformStamped::ConstPtr& msg);
#else
    void msgCallback(const geometry_msgs::PoseStamped::ConstPtr& msg);
#endif
};

#endif // MESREADER_H
