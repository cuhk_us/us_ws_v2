//#define USEMAVROS
#include <iostream>
#include <chrono>
#include <ros/ros.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <nndp_cpp/dpdl/RefGenerator.h>
#include <common_msgs/state.h>
#include <common_msgs/target.h>
#include <std_msgs/Bool.h>
#include <mavlink/v1.0/common/mavlink.h>
#include <tf/transform_broadcaster.h>
#ifdef USEMAVROS
#include "mavros/Mavlink.h"
#include "mavros/utils.h"
namespace messenger = mavros;
#else
#include <com/Mavlink.h>
#include <com/utils.h>
namespace messenger = com;
#endif

#define LOG_TRAJECTORY
RefGenerator *rg=NULL;
common_msgs::target tgt;
ros::Publisher ref_pub;
ros::Publisher ref_state_pub;

bool engaged;
#ifdef LOG_TRAJECTORY
std::ofstream myfile;
#endif

void referenceCallback(const ros::TimerEvent&)
{
    if (!engaged)
        return;

    // Generate the reference state
    RefGenerator::state s = rg->calculateNextCycleReference(tgt);

    //Publish to the drone
    //Publish to the current reference state
    common_msgs::state msgState;
    msgState.pos.x = s.pos.a;
    msgState.pos.y = s.pos.b;
    msgState.pos.z = s.pos.c;

    msgState.vel.x = s.vel.a;
    msgState.vel.y = s.vel.b;
    msgState.vel.z = s.vel.c;

    msgState.acc.x = s.acc.a;
    msgState.acc.y = s.acc.b;
    msgState.acc.z = s.acc.c;

    msgState.yaw.x = s.yaw.a;
    msgState.yaw.y = s.yaw.b;
    msgState.yaw.z = s.yaw.c;

    ref_state_pub.publish(msgState);

#ifdef LOG_TRAJECTORY
    myfile<<s.pos[0]<<" "<<s.pos[1]<<" "<<s.pos[2]<<" "<<
                      s.vel[0]<<" "<<s.vel[1]<<" "<<s.vel[2]<<" "<<
                      s.acc[0]<<" "<<s.acc[1]<<" "<<s.acc[2]<<" "<<s.yaw.a<<std::endl;
#endif
}

void tgtCallback(const common_msgs::target::ConstPtr& msg)
{
    if (msg->engaged)
        engaged = true;
    if (engaged)
    {
        tgt = *msg;
    }
    else
    {
        // When not engaged reset the initial state
        RefGenerator::state ini;
        ini.pos.setData(msg->tgt.x,msg->tgt.y,msg->tgt.z+0.5);
        ini.yaw.setData(msg->yaw_tgt,0.0,0.0);
        rg->setInternalState(ini);

        // and also the target
        tgt = *msg;
    }
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "rt_ref_gen_ddrone");
    ros::NodeHandle nh;
    std::string home_name;
    nh.param<std::string>("/home_name",home_name,"/home/nvidia");
    engaged = false;
#ifdef LOG_TRAJECTORY
    myfile.open(home_name+"/Trajectory.txt");
#endif
    //--- Initialize the ref generator
#ifdef VEL_CONTROL
        std::string str = home_name+"/ctrlpred/vel_1";
#else
        std::string str = home_name+"/ctrlpred/pos_1";
#endif
    rg = new RefGenerator(home_name+"/ctrlpred/jlt_1/yaw/ctrl");
    rg->addController(str);

    ref_pub = nh.advertise<messenger::Mavlink>("mavlink/ros_to_pixhawk", 1);
    ref_state_pub = nh.advertise<common_msgs::state>("rt_ref_gen/current_state",1);
    ros::Subscriber tgt_sub = nh.subscribe("/nndp_cpp/tgt", 1, tgtCallback);
    ros::Timer reference_timer = nh.createTimer(ros::Duration(0.05), referenceCallback);
    ros::spin();


    delete rg;

#ifdef LOG_TRAJECTORY
    myfile.close();
#endif

    return 0;
}
