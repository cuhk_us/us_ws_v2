#ifndef POSCONTROL_H
#define POSCONTROL_H

#include "Controller.h"
class PosControl:public Controller
{
    struct point
    {
        Dpdl::vec3i loc;
        Dpdl::vec3i glb;
    };

public:
    PosControl();
    PosControl(std::string name);
    virtual Dpdl::vec3d act(Dpdl::vec3d s, double tgt);
    virtual double getDt();
private:
    MatrixLoader _loader;
    Eigen::MatrixXd _PI;
    Eigen::MatrixXd _q_bins;
    Eigen::MatrixXd _qdot_bins;
    Eigen::MatrixXd _qdd_bins;
    double _dt;
private:
    point getOppositePoint(const point&p);
    void simulate(double& p, double& v, double& a, const double& u);
};

#endif // POSCONTROL_H
