#ifndef JLTCONTROL_H
#define JLTCONTROL_H

#include "Controller.h"
#include "nndp_cpp/dpdl/JLT.h"
class JLTControl:public Controller
{
public:
    JLTControl(std::string name);
    virtual Dpdl::vec3d act(Dpdl::vec3d s, double tgt);
    virtual double getDt();
private:
    double _dt;
    JLT _gen;
    JLT::TPBVPParam _sp;
    JLT::State _ini, _next;
    JLT::Limit _lim;
};

#endif // JLTCONTROL_H
