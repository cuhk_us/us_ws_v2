#ifndef REFGENERATOR_H
#define REFGENERATOR_H
#include <vector>
#include <nndp_cpp/dpdl/PosControl.h>
#include <nndp_cpp/dpdl/VelControl.h>
#include <nndp_cpp/dpdl/JLTControl.h>
#include <common_msgs/target.h>
#include <string>
//#define VEL_CONTROL
class RefGenerator
{
public:
    struct state
    {
        Dpdl::vec3d pos;
        Dpdl::vec3d vel;
        Dpdl::vec3d acc;
        Dpdl::vec3d yaw;//[0][1][2] for yaw, yaw speed and yaw acceleration
        state()
        {
        }

        state& operator=(state const& rhs)
        {
            pos = rhs.pos;
            vel = rhs.vel;
            acc = rhs.acc;
            yaw = rhs.yaw;
            return *this;
        }
    };

public:
    RefGenerator(std::string yaw_ctrl_str);
    ~RefGenerator();
    void addController(const std::string &name);
    void setInternalState(const state & s);
    state getInternalState() {return _s;}
    state calculateNextCycleReference(const common_msgs::target & tgt);
    double getDt() {return _dt;}

private:
    void progress(double& p, double& v, double& a, const double& target, Controller* ctrl);
    bool checkName(const std::string &name, const std::string &str);
    void extendName(const std::string &name, std::string &hor, std::string &ver);

private:
    std::vector<Controller*> _hor_ctrls;
    std::vector<Controller*> _ver_ctrls;
    JLTControl* _yaw_ctrl;
    double _dt;
    state _s;
};

#endif // REFGENERATOR_H
