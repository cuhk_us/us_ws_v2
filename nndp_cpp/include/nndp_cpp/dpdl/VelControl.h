#ifndef VELCONTROL_H
#define VELCONTROL_H

#include "Controller.h"
class VelControl:public Controller
{
    struct point
    {
        Dpdl::vec2i loc;
        Dpdl::vec2i glb;
    };

public:
    VelControl();
    VelControl(std::string name);
    virtual Dpdl::vec3d act(Dpdl::vec3d cur, double tgt);
    virtual double getDt();
private:
    MatrixLoader _loader;
    Eigen::MatrixXd _PI;
    Eigen::MatrixXd _q_bins;
    Eigen::MatrixXd _qdot_bins;
    double _dt;
private:
    point getOppositePoint(const point&p);
    void simulate(double& p, double& v, double& a, const double& u);
};

#endif // VELCONTROL_H
