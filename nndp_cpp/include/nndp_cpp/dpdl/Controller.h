#ifndef CONTROLLER_H
#define CONTROLLER_H
#include "MatrixLoader.h"
#include "DpdlUtilities.h"
class Controller
{
public:
    Controller();
    virtual Dpdl::vec3d act(Dpdl::vec3d cur, double tgt)=0;
    virtual double getDt()=0;
    virtual ~Controller();

protected:
    void bound(double& in, double min, double max);
    int searchIndex(double val, const Eigen::MatrixXd& bins);
};

#endif // CONTROLLER_H
