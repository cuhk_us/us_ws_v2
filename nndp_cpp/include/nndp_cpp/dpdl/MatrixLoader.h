#ifndef MATRIXLOADER_H
#define MATRIXLOADER_H
#include <nndp_cpp/Eigen/Dense>
#include <iostream>
#include <fstream>
#include <string.h>
class MatrixLoader
{
public:
    MatrixLoader();
    Eigen::MatrixXd loadMatrix(std::string name);
private:
    char *readFileBytes(const char *name, size_t& len);
    char* _bytes;
};

#endif // MATRIXLOADER_H
