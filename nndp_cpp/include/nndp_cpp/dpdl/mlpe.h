#ifndef MLPE_H
#define MLPE_H
#include <nndp_cpp/Eigen/Dense>
#include <nndp_cpp/dpdl/cnpy.h>
#include <nndp_cpp/dpdl/DpdlUtilities.h>
#include <list>
#include <mutex>
class Mlpe
{
public:
    struct nRes
    {
        double tgt;
        float u_sq;
        std::list<double> pList;
    };

public:
    Mlpe(std::string[], int layer_num);
    ~Mlpe();
    nRes evaluate(const Dpdl::vec3d &ini, const double &ptgt, const int &effective_point_size);
    int getInputSize() {return _input_size;}
    int getOutputSize() {return _output_size;}

private:
    int _layer_num;
    int _input_size;
    int _output_size;
    Eigen::MatrixXf *_W,*_b;
    Eigen::VectorXf *_x;
    Eigen::MatrixXf loadEigenMatrix(cnpy::NpyArray arr);
    std::mutex _lock;
};

#endif // MLPE_H
