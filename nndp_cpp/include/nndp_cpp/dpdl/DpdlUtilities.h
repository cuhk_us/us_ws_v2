#ifndef DPDLUTILITIES_H
#define DPDLUTILITIES_H
#include <assert.h>
#include <cstdlib>
namespace Dpdl
{
template <typename T>
struct vec3
{
    T a,b,c;
    vec3():a(0.0),b(0.0),c(0.0)
    {

    }

    vec3(T a_, T b_, T c_):a(a_),b(b_),c(c_)
    {

    }

    T square()
    {
        return a*a+b*b+c*c;
    }

    vec3 operator+(vec3 const& rhs) const
    {
        vec3 tmp;
        tmp.a = a + rhs.a;
        tmp.b = b + rhs.b;
        tmp.c = c + rhs.c;
        return tmp;
    }

    vec3 operator-(vec3 const& rhs) const
    {
        vec3 tmp;
        tmp.a = a - rhs.a;
        tmp.b = b - rhs.b;
        tmp.c = c - rhs.c;
        return tmp;
    }

    vec3 operator*(T const& rhs) const
    {
        vec3 tmp;
        tmp.a = a * rhs;
        tmp.b = b * rhs;
        tmp.c = c * rhs;
        return tmp;
    }

    vec3 operator/(T const& rhs) const
    {
        vec3 tmp;
        tmp.a = a / rhs;
        tmp.b = b / rhs;
        tmp.c = c / rhs;
        return tmp;
    }

    vec3& operator=(vec3 const& rhs)
    {
        a = rhs.a;
        b = rhs.b;
        c = rhs.c;
        return *this;
    }

    bool operator==(vec3 const& rhs) const
    {
        if (a==rhs.a && b==rhs.b && c==rhs.c)
            return true;
        else
            return false;
    }

    T& at(int i)
    {
        switch (i) {
        case 0:
            return a;
        case 1:
            return b;
        case 2:
            return c;
        default:
            assert(false);
        }
    }

    T& operator[](int i)
    {
        return this->at(i);
    }

    const T& const_at(int i) const
    {
        switch (i) {
        case 0:
            return a;
        case 1:
            return b;
        case 2:
            return c;
        default:
            assert(false);
        }
    }

    void setData(T a_, T b_, T c_)
    {
        a = a_;
        b = b_;
        c = c_;
    }
};
//---
template <typename T>
struct vec2
{
    T a,b;
    vec2():a(0.0),b(0.0)
    {

    }

    T square()
    {
        return a*a+b*b;
    }

    vec2 operator+(vec2 const& rhs) const
    {
        vec2 tmp;
        tmp.a = a + rhs.a;
        tmp.b = b + rhs.b;
        return tmp;
    }

    vec2 operator-(vec2 const& rhs) const
    {
        vec2 tmp;
        tmp.a = a - rhs.a;
        tmp.b = b - rhs.b;
        return tmp;
    }

    vec2 operator*(T const& rhs) const
    {
        vec2 tmp;
        tmp.a = a * rhs;
        tmp.b = b * rhs;
        return tmp;
    }

    vec2 operator/(T const& rhs) const
    {
        vec2 tmp;
        tmp.a = a / rhs;
        tmp.b = b / rhs;
        return tmp;
    }

    vec2& operator=(vec2 const& rhs)
    {
        a = rhs.a;
        b = rhs.b;
        return *this;
    }

    bool operator==(vec2 const& rhs) const
    {
        if (a==rhs.a && b==rhs.b)
            return true;
        else
            return false;
    }

    T& at(int i)
    {
        switch (i) {
        case 0:
            return a;
        case 1:
            return b;
        default:
            assert(false);
        }
    }

    T& operator[](int i)
    {
        return this->at(i);
    }

    const T& const_at(int i) const
    {
        switch (i) {
        case 0:
            return a;
        case 1:
            return b;
        default:
            assert(false);
        }
    }

    void setData(T a_, T b_)
    {
        a = a_;
        b = b_;
    }
};
//---
typedef vec3<double> vec3d;
typedef vec3<int> vec3i;
typedef vec2<double> vec2d;
typedef vec2<int> vec2i;

//---
static double fRand(double fMin, double fMax, unsigned int *seedp)
{
    double f = (double)rand_r(seedp) / RAND_MAX;
    return fMin + f * (fMax - fMin);
}
//---
static int iRand(int min, int max, unsigned int *seedp)
{
    return rand_r(seedp)%(max-min + 1) + min;
}
}
#endif // DPDLUTILITIES_H
