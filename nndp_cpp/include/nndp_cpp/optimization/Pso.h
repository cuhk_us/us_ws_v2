#ifndef PSO_H
#define PSO_H
#include <nndp_cpp/optimization/Evaluator.h>
#include <nndp_cpp/dpdl/mlpe.h>
#include <nndp_cpp/optimization/threadpool.h>
#include "nndp_cpp/optimization/PosPredictor.h"
#include "nndp_cpp/optimization/VelPredictor.h"
#include "nndp_cpp/optimization/JLTPredictor.h"

class Pso
{
public:
    struct TargetInfo
    {
        Dpdl::vec3d target;
        double yaw_target;
        double cost;
        bool collision;
        TargetInfo():yaw_target(0.0),cost(std::numeric_limits<double>::infinity()),collision(true)
        {

        }

        void reset()
        {
            cost = std::numeric_limits<double>::infinity();
            collision = true;
        }
    };

    struct Particle
    {
        Dpdl::vec3i current_tgt;
        Dpdl::vec3i best_tgt;
        Dpdl::vec3i current_vel;
        double best_cost;
        bool best_collision;
        unsigned int rand_seed;
        Particle():best_cost(std::numeric_limits<double>::infinity()),best_collision(true)
        {
            static unsigned int i = 0;
            rand_seed = i;
            i++;
        }

        void reset()
        {
            best_cost = std::numeric_limits<double>::infinity();
            best_collision = true;
        }
    };

public:
    Pso(int batch_size, int epoch_size,LinDistMap* map);
    ~Pso();
    void addPredictor(const std::string &name, const int hor_num, const int ver_num, const PosPredictor::range &ran);
    void selectTgt(const RefGenerator::state &ini, const Dpdl::vec3d &pre_tgt,
                         const Dpdl::vec3d & goal, TargetInfo &tgt);

private:
    void randomInitialize(const RefGenerator::state &ini);
    void par_eva(unsigned int i, double w, RefGenerator::state ini,
                 Dpdl::vec3d pre_tgt, Dpdl::vec3d goal, bool isFirst);
    bool checkName(const std::string &name, const std::string &str);
    void extendName(const std::string &name, std::string hor[], std::string ver[], int hor_num, int ver_num);

private:
    ThreadPool _pool;
    LinDistMap* _map;
    Evaluator* _evaluator;
    PosPredictor* _predictor;
    int _batch_size;
    int _epouch_size;
    Particle* _P;
    Particle _best_P;
};

#endif // PSO_H
