#ifndef VELPREDICTOR_H
#define VELPREDICTOR_H
#include <nndp_cpp/optimization/MotionPredictor.h>

class VelPredictor:public MotionPredictor
{
public:
  VelPredictor(std::string hor_names[], std::string ver_names[], int hor_layer_num, int ver_layer_num);
  ~VelPredictor();
  virtual Evaluator::nnResult nnPredict(const RefGenerator::state &ini, const Dpdl::vec3d &tgt);
private:
    Mlpe _nn_hor;
    Mlpe _nn_ver;
    float* _nn_in_ptr[3];
    float* _nn_out_ptr[3];
    int _in_size;
    int _out_size;
    int _effective_point_size;
    RefGenerator::state _ini;
    Dpdl::vec3d _tgt;
};

#endif // VELPREDICTOR_H
