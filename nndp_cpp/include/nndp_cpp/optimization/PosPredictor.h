#ifndef POSPREDICTOR_H
#define POSPREDICTOR_H
#include <nndp_cpp/optimization/MotionPredictor.h>
#include <nndp_cpp/optimization/threadpool.h>
class PosPredictor:public MotionPredictor
{
public:
    struct range
    {
        double loc_h, loc_v;
        Dpdl::vec3d glb_min, glb_max;
    };

public:
    PosPredictor(std::string hor_names[], std::string ver_names[], int hor_layer_num, int ver_layer_num,
                 ThreadPool *pool, const range &ran);
    ~PosPredictor();
    virtual Evaluator::nnResult nnPredict(const Dpdl::vec3i &idx) const;
    void setInitialState(const RefGenerator::state &ini);
    void limitRange(Dpdl::vec3i& tgt) const;
    void bound(int& val, const int& min, const int& max) const;
    Dpdl::vec3d idx2pos(const Dpdl::vec3i &idx) const
    {
        Dpdl::vec3d pos;
        for (int i=0; i<3; i++)
            pos[i] = idx2pos(i,idx.const_at(i));
        return pos;
    }
    Dpdl::vec3i pos2idx(const Dpdl::vec3d &pos) const
    {
        Dpdl::vec3i idx;
        for (int i=0; i<3; i++)
            idx[i] = pos2idx(i,pos.const_at(i));
        return idx;
    }
    int xSize() const
    {
        return _x_sz;
    }
    int ySize() const
    {
        return _y_sz;
    }
    int zSize() const
    {
        return _z_sz;
    }
private:
    void genTraj(const double &tgt, int d, int i);
    void updateRangeBound();
    double idx2pos(int dim, int idx) const
    {
        return ((double)(_d_ini_pos.const_at(dim)+_shift[dim]+idx))*_c2d_f;
    }
    int pos2idx(int dim, double pos) const
    {
        return floor(pos/_c2d_f+0.5)-_d_ini_pos.const_at(dim)-_shift[dim];
    }
private:
    static constexpr double _c2d_f = 0.25;
    ThreadPool *_pool;
    Mlpe *_nn_hors[8];
    Mlpe *_nn_vers[8];
    int _in_size;
    int _out_size;
    int _effective_point_size;
    RefGenerator::state _ini;
    Dpdl::vec3i _d_ini_pos;
    Dpdl::vec3d _tgt;
    Mlpe::nRes *xLib, *yLib, *zLib;
    int _x_sz,_y_sz,_z_sz;
    Dpdl::vec3d _glb_min, _glb_max;
    Dpdl::vec3i _lb, _ub;
    int _shift[3];
};

#endif // POSPREDICTOR_H
