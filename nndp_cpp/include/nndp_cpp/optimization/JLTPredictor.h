#ifndef JLTPREDICTOR_H
#define JLTPREDICTOR_H
#include <nndp_cpp/optimization/MotionPredictor.h>
#include "nndp_cpp/dpdl/JLT.h"
class JLTPredictor:public MotionPredictor
{
public:
    JLTPredictor(std::string hor_name, std::string ver_name);
    ~JLTPredictor();
    virtual Evaluator::nnResult nnPredict(const RefGenerator::state &s, const Dpdl::vec3d &tgt);
private:
    JLT _gen;
    JLT::TPBVPParam _sp[3];
    JLT::State _ini;
    JLT::Limit _lim[3];
    std::list<Dpdl::vec3d> _acc_history;
private:
    double calcUsq(const Dpdl::vec3d &cur, const Dpdl::vec3d &next, const double &dt);
};

#endif // JLTPREDICTOR_H
