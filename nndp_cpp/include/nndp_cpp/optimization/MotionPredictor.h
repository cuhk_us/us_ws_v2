#ifndef MOTIONPREDICTOR_H
#define MOTIONPREDICTOR_H
#include <nndp_cpp/dpdl/mlpe.h>
#include <nndp_cpp/dpdl/RefGenerator.h>
#include <nndp_cpp/optimization/Evaluator.h>

class MotionPredictor
{
public:
    MotionPredictor();
    virtual Evaluator::nnResult nnPredict(const Dpdl::vec3i &idx) const = 0;
    virtual ~MotionPredictor();
};

#endif // MOTIONPREDICTOR_H
