#ifndef EVALUATOR_H
#define EVALUATOR_H
#include "nndp_cpp/dpdl/RefGenerator.h"
#include "edt/LinDistMap.h"
#include "nndp_cpp/dpdl/mlpe.h"

//#define CHECK_EVALUATE_DETAIL
class MotionPredictor;
class Evaluator
{
public:
    struct evaResult
    {
        bool collision;
        double cost;
#ifdef CHECK_EVALUATE_DETAIL
        std::list<DevGeo::pos> posList;
        std::list<std::list<DevGeo::coord>> coords;
#endif
        evaResult():collision(false),cost(0.0){}
    };

    struct nnResult
    {
        Dpdl::vec3d tgt;
        float u_sq;
        std::list<DevGeo::pos> posList;
    };

    Evaluator(LinDistMap* map);
    ~Evaluator();
    void setWeight(double w_obs, double w_R, double w_Q, double w_Qf, double w_prev, int plan_horizon);
    evaResult evaluate(const nnResult &estimation, const Dpdl::vec3d &pre_tgt,
                       const Dpdl::vec3d &goal, const RefGenerator::state &ini) const;

private:
    LinDistMap* _map;
    double _w_obs,_w_R,_w_Qf,_w_prev,_w_Q;
    int _plan_horizon;

private:
    void addObstacleCost(const std::list<DevGeo::pos> &posList, evaResult &result, const double &curr_yaw) const;
    void addTargetCost(const std::list<DevGeo::pos> &posList, const Dpdl::vec3d &goal, evaResult &result) const;
    void addActionCost(const double &u_sq, const Dpdl::vec3d &tgt,
                       const Dpdl::vec3d &pre_tgt, evaResult &result) const;
};

#endif // EVALUATOR_H
