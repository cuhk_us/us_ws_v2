#include "nndp_cpp/optimization/JLTPredictor.h"

JLTPredictor::JLTPredictor(std::string hor_name, std::string ver_name)
{
    double tmp;
    hor_name.append("/JLTSetting.txt");
    ver_name.append("/JLTSetting.txt");
    JLT::loadTimeAndLimFromFile(hor_name,tmp,_lim[0]);
    JLT::loadTimeAndLimFromFile(hor_name,tmp,_lim[1]);
    JLT::loadTimeAndLimFromFile(ver_name,tmp,_lim[2]);
}

JLTPredictor::~JLTPredictor()
{

}

Evaluator::nnResult JLTPredictor::nnPredict(const RefGenerator::state &s, const Dpdl::vec3d &tgt)
{
    // Declare the output
    Evaluator::nnResult result;

    // Calculate the trajectory for the three axis
    for (int i=0;i<3;i++)
    {
        // Set the initial state
        _ini.p = s.pos.const_at(i);
        _ini.v = s.vel.const_at(i);
        _ini.a = s.acc.const_at(i);

        // Calculate the trajectory reference
        bool ok = _gen.solveTPBVP(tgt.const_at(i),0,_ini,_lim[i],_sp[i]);
        if (!ok)
        {
            assert(0);
        }
    }

    JLT::State ns;
    DevGeo::pos p;
    Dpdl::vec3d acc_cur, acc_next;
    double dt = 0.2;
    // Put the trajectory into the result
    for (double t=0;t<8;t+=dt)
    {
        for (int i=0;i<3;i++)
        {
            ns =_gen.TPBVPRefGen(_sp[i],t);
            p.at(i) = ns.p;
            // Log down the three axis acceleration at this moment
            acc_cur.at(i) = ns.a;
        }
        _acc_history.push_back(acc_cur);
        result.posList.push_back(p);
    }

    // Calculate u_sq
    result.u_sq = 0;
    if (_acc_history.size()>0)
    {
        acc_cur = _acc_history.front();
        _acc_history.pop_front();
        while (_acc_history.size()>0)
        {
            acc_next = _acc_history.front();
            _acc_history.pop_front();
            // actual calculation here
            result.u_sq += calcUsq(acc_cur, acc_next, dt);
            acc_cur = acc_next;
        }
    }
    return result;
}

double JLTPredictor::calcUsq(const Dpdl::vec3d &cur, const Dpdl::vec3d &next, const double &dt)
{
    double usq = 0;
    double jerk = 0;
    for (int i=0;i<3;i++)
    {
        jerk = (next.const_at(i) - cur.const_at(i))/dt;
        usq += jerk*jerk*dt;
    }
    return usq;
}
