#include "nndp_cpp/optimization/VelPredictor.h"

VelPredictor::VelPredictor(std::string hor_names[], std::string ver_names[], int hor_layer_num, int ver_layer_num):
  _nn_hor(hor_names, hor_layer_num),
  _nn_ver(ver_names, ver_layer_num)
{
  if(_nn_hor.getInputSize() == _nn_ver.getInputSize() &&
          _nn_hor.getOutputSize() == _nn_ver.getOutputSize())
  {
      _in_size = _nn_hor.getInputSize();
      _out_size = _nn_hor.getOutputSize();
  }
  else
  {
      assert(0 && "The input or output dimension of hor/ver NN mismatch.\n");
  }
  _effective_point_size = _out_size - 1;

  for (int i=0;i<3;i++)
  {
      _nn_in_ptr[i] = new float[_in_size];
      _nn_out_ptr[i] = new float[_out_size];
  }
}

VelPredictor::~VelPredictor()
{
  for (int i=0;i<3;i++)
  {
      if (_nn_in_ptr[i]!=nullptr)
          delete [] _nn_in_ptr[i];

      if (_nn_out_ptr[i]!=nullptr)
          delete [] _nn_out_ptr[i];
  }
}

Evaluator::nnResult VelPredictor::nnPredict(const RefGenerator::state &ini, const Dpdl::vec3d &tgt)
{
    Evaluator::nnResult result;
    DevGeo::pos p;
    // Get initial state
    for (int i=0; i<3; i++)
    {
        _nn_in_ptr[i][0] = tgt.const_at(i);
        _nn_in_ptr[i][1] = ini.vel.const_at(i);
        _nn_in_ptr[i][2] = ini.acc.const_at(i);
    }

    // Horizontal
//    for (int i=0;i<2;i++)
//    {
//        _nn_hor.evaluate(_nn_in_ptr[i],_nn_out_ptr[i]);
//    }
//    // Vertical
//    _nn_ver.evaluate(_nn_in_ptr[2],_nn_out_ptr[2]);

    // Put into the position list
    // first is the ini position
    p.x = ini.pos.const_at(0);
    p.y = ini.pos.const_at(1);
    p.z = ini.pos.const_at(2);
    result.posList.push_back(p);
    // the nn predictions
    for (int i=0;i<_effective_point_size;i++)
    {
        p.x = _nn_out_ptr[0][i]+ini.pos.const_at(0);
        p.y = _nn_out_ptr[1][i]+ini.pos.const_at(1);
        p.z = _nn_out_ptr[2][i]+ini.pos.const_at(2);
        result.posList.push_back(p);
    }

    // Setup the u_square from 3 axes, the last in each _nn_out_ptr[i][] is the u_square
    result.u_sq = 0;
    for (int i=0; i<3; i++)
    {
        result.u_sq += _nn_out_ptr[i][_out_size-1];
    }
    return result;
}
