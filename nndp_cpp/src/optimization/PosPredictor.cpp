#include "nndp_cpp/optimization/PosPredictor.h"
#include <algorithm>

PosPredictor::PosPredictor(std::string hor_names[], std::string ver_names[], int hor_layer_num, int ver_layer_num, ThreadPool *pool, const range &ran):
    _pool(pool)
{
    _x_sz = floor(ran.loc_h*2/_c2d_f);
    _y_sz = _x_sz;
    _z_sz = floor(ran.loc_v*2/_c2d_f);
    _glb_min = ran.glb_min;
    _glb_max = ran.glb_max;

    for(int i=0; i<8;i++)
    {
        _nn_hors[i] = new Mlpe(hor_names, hor_layer_num);
        _nn_vers[i] = new Mlpe(ver_names, ver_layer_num);
    }

    if(_nn_hors[0]->getInputSize() == _nn_vers[0]->getInputSize() &&
            _nn_hors[0]->getOutputSize() == _nn_vers[0]->getOutputSize())
    {
        _in_size = _nn_hors[0]->getInputSize();
        _out_size = _nn_hors[0]->getOutputSize();
    }
    else
    {
        assert(0 && "The input or output dimension of hor/ver NN mismatch.\n");
    }
    _effective_point_size = _out_size - 1;

    xLib = new Mlpe::nRes[_x_sz+1];
    yLib = new Mlpe::nRes[_y_sz+1];
    zLib = new Mlpe::nRes[_z_sz+1];

    _shift[0]=-_x_sz/2;
    _shift[1]=-_y_sz/2;
    _shift[2]=-_z_sz/2;

    // Order maters here, can only call the pos2idx/idx2pos function after setting _shift
    updateRangeBound();
}

PosPredictor::~PosPredictor()
{  
    for(int i=0;i<8;i++)
    {
        delete _nn_hors[i];
        delete _nn_vers[i];
    }
    delete [] xLib;
    delete [] yLib;
    delete [] zLib;
}

Evaluator::nnResult PosPredictor::nnPredict(const Dpdl::vec3i &id) const
{
    //retrive the trajectory
    Evaluator::nnResult result;
    DevGeo::pos p;

    std::list<double>::const_iterator ix,iy,iz;
    ix = xLib[id.a].pList.begin();
    iy = yLib[id.b].pList.begin();


    for (iz=zLib[id.c].pList.begin();iz!=zLib[id.c].pList.end();iz++)
    {
        p.x = *ix;
        p.y = *iy;
        p.z = *iz;
        result.posList.push_back(p);
        ix++;
        iy++;
    }

    // Setup the u_square from 3 axes
    result.u_sq = xLib[id.a].u_sq+yLib[id.b].u_sq+zLib[id.c].u_sq;

    // Set the target
    result.tgt[0]=xLib[id.a].tgt;
    result.tgt[1]=yLib[id.b].tgt;
    result.tgt[2]=zLib[id.c].tgt;

    return result;
}

void PosPredictor::setInitialState(const RefGenerator::state &ini)
{
    _ini = ini;
    for (int i=0;i<3;i++)
        _d_ini_pos.at(i) = floor(ini.pos.const_at(i) / _c2d_f + 0.5);

    // Update the range bounds
    updateRangeBound();

    double target;
    // Generate trajectory library

    // Horizontal
    for (int i=0;i<=_x_sz;i++)
    {
        target = idx2pos(0,i);
        _pool->enqueue(std::bind(&PosPredictor::genTraj,this,target,0,i));
    }

    for (int i=0;i<=_y_sz;i++)
    {
        target = idx2pos(1,i);
        _pool->enqueue(std::bind(&PosPredictor::genTraj,this,target,1,i));
    }

    // Vertical
    for (int i=0;i<=_z_sz;i++)
    {
        target = idx2pos(2,i);
        _pool->enqueue(std::bind(&PosPredictor::genTraj,this,target,2,i));
    }
    _pool->waitFinished();
}

void PosPredictor::genTraj(const double &tgt, int d, int i)
{
    Dpdl::vec3d pva(_ini.pos.const_at(d), _ini.vel.const_at(d), _ini.acc.const_at(d));

    // Horizontal
    switch (d)
    {
    case 0:
        xLib[i]=_nn_hors[i%8]->evaluate(pva,tgt,_effective_point_size);
        break;
    case 1:
        yLib[i]=_nn_hors[i%8]->evaluate(pva,tgt,_effective_point_size);
        break;
    case 2:
        zLib[i]=_nn_vers[i%8]->evaluate(pva,tgt,_effective_point_size);
        break;
    }
}


void PosPredictor::limitRange(Dpdl::vec3i &tgt) const
{
#ifdef VEL_CONTROL
    bound(tgt[0],-_range_hor,_range_hor);
    bound(tgt[1],-_range_hor,_range_hor);
    bound(tgt[2],-_range_ver,_range_ver);
#else
    bound(tgt[0],_lb.a,_ub.a);
    bound(tgt[1],_lb.b,_ub.b);
    bound(tgt[2],_lb.c,_ub.c);
#endif
}

void PosPredictor::bound(int &val, const int &min, const int &max) const
{
    if (val>max)
    {
        val = max;
    }
    else if (val<min)
    {
        val = min;
    }
}

void PosPredictor::updateRangeBound()
{
    // x axis
    _lb.a=std::min(_x_sz, std::max(0,pos2idx(0,_glb_min.a)));
    _ub.a=std::max(0, std::min(_x_sz,pos2idx(0,_glb_max.a)));

    // y axis
    _lb.b=std::min(_y_sz, std::max(0,pos2idx(1,_glb_min.b)));
    _ub.b=std::max(0, std::min(_y_sz,pos2idx(1,_glb_max.b)));

    // z axis
    _lb.c=std::min(_z_sz, std::max(0,pos2idx(2,_glb_min.c)));
    _ub.c=std::max(0, std::min(_z_sz,pos2idx(2,_glb_max.c)));
}
