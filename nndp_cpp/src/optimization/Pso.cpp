#include "nndp_cpp/optimization/Pso.h"
#include <nndp_cpp/optimization/OptimizationUtilities.h>

Pso::Pso(int batch_size, int epoch_size, LinDistMap *map):
    _batch_size(batch_size),
    _epouch_size(epoch_size)
{
    _map = map;
    _P = new Particle[_batch_size];

    _evaluator = new Evaluator(_map);
}

Pso::~Pso()
{
    delete [] _P;
    delete _evaluator;
    delete _predictor;
}

void Pso::addPredictor(const std::string &name, const int hor_num, const int ver_num, const PosPredictor::range &ran)
{
    std::string *hor, *ver;
    hor = new std::string [hor_num*2];
    ver = new std::string [ver_num*2];
    extendName(name,hor,ver,hor_num,ver_num);
    _predictor = new PosPredictor(hor, ver, hor_num, ver_num, &_pool, ran);

    delete [] hor;
    delete [] ver;
}

void Pso::selectTgt(const RefGenerator::state &ini, const Dpdl::vec3d &pre_tgt,
                               const Dpdl::vec3d & goal, TargetInfo &tgt)
{
    Evaluator::evaResult r;
    _predictor->setInitialState(ini);

    // Project g into a local range
    Dpdl::vec3d local_g = goal - ini.pos;
    if (sqrt(local_g.square())>10)
        local_g = local_g/sqrt(local_g.square())*10 + ini.pos;

    // Random initiallization
    randomInitialize(ini);

    // Set the weights for the cost function
    DevGeo::pos ini_map_pos;
    for (int i=0;i<3;i++)
        ini_map_pos.at(i) = ini.pos.const_at(i);

    float ini_dist = _map->distAt(_map->pos2coord(ini_map_pos),(float)EDTMap::MAX_RANGE);
    if (ini_dist*_map->getGridStep() <= Optim::rTooClose)
    {
#ifdef VEL_CONTROL
        _evaluator->setWeight(1,0,0,0,0,30);
#else
        _evaluator->setWeight(1,0,0,0,0,30);
#endif
    }
    else
    {
#ifdef VEL_CONTROL
        _evaluator->setWeight(4,0.25,2,3,0.5,30);
#else
        _evaluator->setWeight(4,0.25,2,3,0.5,30);
#endif
    }

    // Reset the global best to the previous target
    Dpdl::vec3i pre_tgt_idx = _predictor->pos2idx(pre_tgt);
    _predictor->limitRange(pre_tgt_idx);
    Evaluator::nnResult nnr = _predictor->nnPredict(pre_tgt_idx);
    r = _evaluator->evaluate(nnr, pre_tgt, goal, ini);
    _best_P.best_tgt = pre_tgt_idx;
    _best_P.best_cost = r.cost;
    _best_P.best_collision = r.collision;

    // Start the swarm movement
    for (int e=0; e<_epouch_size;e++)
    {
        double w = 0.95-(0.95-0.4)/(double)_epouch_size*(double)e;

        bool isFirst = (e==0);
        for (int i=0;i<_batch_size;i++)
        {
            _pool.enqueue(std::bind(&Pso::par_eva,this,i,w,ini,pre_tgt,goal,isFirst));
        }

        // **************************************************************
        // It blocks the main thread until all reading of map is finished,
        // so if the map updating is in the main thread,
        // this will be thread safe.
        // **************************************************************
        _pool.waitFinished();

        for (int i=0; i<_batch_size;i++)
        {
            if (!_P[i].best_collision && (_P[i].best_cost <_best_P.best_cost || _best_P.best_collision))
            {
                _best_P.best_cost = _P[i].best_cost;
                _best_P.best_tgt = _P[i].best_tgt;
                _best_P.best_collision = _P[i].best_collision;
            }
        }
    }

    tgt.collision = _best_P.best_collision;
    tgt.cost = _best_P.best_cost;
    tgt.target = _predictor->idx2pos(_best_P.best_tgt);
}

void Pso::randomInitialize(const RefGenerator::state &ini)
{
    for (int i=0;i<_batch_size;i++)
    {
        _P[i].reset();
#ifdef VEL_CONTROL
        _P[i].current_tgt.setData(Dpdl::fRand(-_range_hor, _range_hor,&_P[i].rand_seed),
                                  Dpdl::fRand(-_range_hor, _range_hor,&_P[i].rand_seed),
                                  Dpdl::fRand(-_range_ver, _range_ver,&_P[i].rand_seed));
#else
        _P[i].current_tgt.setData(Dpdl::iRand(0, _predictor->xSize(),&_P[i].rand_seed),
                                  Dpdl::iRand(0, _predictor->ySize(),&_P[i].rand_seed),
                                  Dpdl::iRand(0, _predictor->zSize(),&_P[i].rand_seed));
#endif
        _predictor->limitRange(_P[i].current_tgt);

        _P[i].current_vel.setData(Dpdl::iRand(-4,4,&_P[i].rand_seed),
                                  Dpdl::iRand(-4,4,&_P[i].rand_seed),
                                  Dpdl::iRand(-4,4,&_P[i].rand_seed));

        _P[i].best_tgt = _P[i].current_tgt;

    }
}

void Pso::par_eva(unsigned int i, double w, RefGenerator::state ini,
                  Dpdl::vec3d pre_tgt, Dpdl::vec3d goal, bool isFirst)
{
    if (!isFirst)
    {
        // Update velocity
        double r1 = Dpdl::fRand(0,1,&_P[i].rand_seed);
        double r2 = Dpdl::fRand(0,1,&_P[i].rand_seed);

        for (int k=0;k<3;k++)
        {
            _P[i].current_vel[k] = (double)_P[i].current_vel[k]*w
                    -((double)(_P[i].current_tgt[k]-_P[i].best_tgt[k]))*r1
                    -((double)(_P[i].current_tgt[k]-_best_P.best_tgt[k]))*r2;

            if (_P[i].current_vel[k] < -8)
                _P[i].current_vel[k] = -8;
            else if (_P[i].current_vel[k] > 8)
                _P[i].current_vel[k]=8;
        }

        // Update the particle position
        _P[i].current_tgt = _P[i].current_tgt+_P[i].current_vel;
        // Limit the particle's range
        _predictor->limitRange(_P[i].current_tgt);
    }
    Evaluator::nnResult nnr = _predictor->nnPredict(_P[i].current_tgt);
    Evaluator::evaResult r = _evaluator->evaluate(nnr, pre_tgt, goal, ini);
    if (!r.collision && (r.cost < _P[i].best_cost || _P[i].best_collision))
    {
        _P[i].best_cost = r.cost;
        _P[i].best_tgt = _P[i].current_tgt;
        _P[i].best_collision = r.collision;
    }
}

bool Pso::checkName(const std::string &name, const std::string &str)
{
    if (name.find(str) != std::string::npos)
        return true;
    else
        return false;
}

void Pso::extendName(const std::string &name, std::string hor[], std::string ver[], int hor_num, int ver_num)
{
    // horizontal part
    for (int i = 0; i < hor_num*2; i++)
    {
        std::string str = name+"/hor/pred/array_";
        str.append(static_cast<std::ostringstream*>( &(std::ostringstream() << i) )->str());
        str.append(".npy");
        hor[i] = str;
    }

    // vertical part
    for (int i = 0; i < ver_num*2; i++)
    {
        std::string str = name+"/ver/pred/array_";
        str.append(static_cast<std::ostringstream*>( &(std::ostringstream() << i) )->str());
        str.append(".npy");
        ver[i] = str;
    }
}
