#include "nndp_cpp/optimization/Evaluator.h"
#include "nndp_cpp/optimization/OptimizationUtilities.h"

Evaluator::Evaluator(LinDistMap *map)
{
    _map = map;
    _w_obs = 1.0;
    _w_R = 0.25;
    _w_Q = 1.0;
    _w_Qf = 1.0;
    _w_prev = 0.55;
    _plan_horizon = 30;
}

Evaluator::~Evaluator()
{

}

Evaluator::evaResult Evaluator::evaluate(const nnResult &estimation, const Dpdl::vec3d &pre_tgt,
                                         const Dpdl::vec3d &goal, const RefGenerator::state &ini) const
{
    evaResult result;

    addObstacleCost(estimation.posList, result, ini.yaw.a);
    addTargetCost(estimation.posList, goal, result);
    addActionCost(estimation.u_sq,estimation.tgt,pre_tgt,result);

    return result;
}

void Evaluator::setWeight(double w_obs, double w_R, double w_Q, double w_Qf, double w_prev,int plan_horizon)
{
    _w_obs = w_obs;
    _w_R = w_R;
    _w_Q = w_Q;
    _w_Qf = w_Qf;
    _w_prev = w_prev;
    _plan_horizon = plan_horizon;
}

void Evaluator::addObstacleCost(const std::list<DevGeo::pos> &posList, evaResult &result, const double &curr_yaw) const
{
    // Get the map coordinates of the trajectory
    double dist_cost = 0;
    bool collision = false;
    _map->evaTrajectory(posList,Optim::rTooClose,Optim::rVehicle,dist_cost,collision,curr_yaw);
    result.collision = collision;
    result.cost += _w_obs*dist_cost;
}

void Evaluator::addTargetCost(const std::list<DevGeo::pos> &posList, const Dpdl::vec3d &goal, evaResult &result) const
{
    double goal_dist = 0;
    double xerr,yerr,zerr;
    int k = 0;
    std::list<DevGeo::pos>::const_iterator it;
    for (it=posList.begin();it!=posList.end();it++)
    {
        xerr = it->x - goal.const_at(0);
        yerr = it->y - goal.const_at(1);
        zerr = it->z - goal.const_at(2);
        if (k == _plan_horizon)
        {
            //terminal cost:
            goal_dist += _w_Qf*sqrt(xerr*xerr + yerr*yerr + 5*zerr*zerr);
            break;
        }
        else
        {
            //integration part:
            goal_dist += _w_Q*sqrt(xerr*xerr + yerr*yerr + 5*zerr*zerr);
        }
        k++;
    }
    result.cost += goal_dist;
}

void Evaluator::addActionCost(const double &u_sq, const Dpdl::vec3d &tgt,
                           const Dpdl::vec3d &pre_tgt, evaResult &result) const
{
    Dpdl::vec3d delta_tgt = pre_tgt - tgt;
    result.cost += _w_R*u_sq + _w_prev*sqrt(delta_tgt.square());
}
