TEMPLATE = app
CONFIG += console c++11 thread
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp \
#    ../../edt/src/BucketQueue.cpp \
    ../../edt/src/MapBase.cpp \
    ../../edt/src/LinDistMap.cpp \
    ../../edt/src/PbtyMap.cpp \
    ../../edt/src/MapUpdater.cpp \
    dpdl/cnpy.cpp \
    dpdl/Controller.cpp \
    dpdl/MatrixLoader.cpp \
    dpdl/mlpe.cpp \
    dpdl/RefGenerator.cpp \
    dpdl/PosControl.cpp \
    dpdl/VelControl.cpp \
    dpdl/JLTControl.cpp \
    dpdl/JLT.cpp \
    optimization/Evaluator.cpp\
    optimization/Pso.cpp\
    optimization/MotionPredictor.cpp\
    optimization/PosPredictor.cpp
#    optimization/VelPredictor.cpp\
#    optimization/JLTPredictor.cpp

HEADERS += \
#    ../../edt/include/edt/BucketQueue.h \
    ../../edt/include/edt/MapBase.h \
    ../../edt/include/edt/EDTMapUtilities.h \
    ../../edt/include/edt/LinDistMap.h \
    ../../edt/include/PbtyMap.h \
    ../../edt/include/edt/MapUpdater.h \
    ../../edt/include/edt/EDTMapUtilities.h\
    ../include/nndp_cpp/dpdl/cnpy.h \
    ../include/nndp_cpp/dpdl/Controller.h \
    ../include/nndp_cpp/dpdl/MatrixLoader.h \
    ../include/nndp_cpp/dpdl/mlpe.h \
    ../include/nndp_cpp/dpdl/RefGenerator.h \
    ../include/nndp_cpp/dpdl/DpdlUtilities.h \
    ../include/nndp_cpp/optimization/Evaluator.h\
    ../include/nndp_cpp/optimization/Pso.h \
    ../include/nndp_cpp/optimization/OptimizationUtilities.h\
    ../include/nndp_cpp/optimization/MotionPredictor.h\
    ../include/nndp_cpp/optimization/threadpool.h\
    ../include/nndp_cpp/optimization/PosPredictor.h\
#    ../include/nndp_cpp/optimization/VelPredictor.h\
#    ../include/nndp_cpp/optimization/JLTPredictor.h\
    ../include/nndp_cpp/dpdl/PosControl.h \
    ../include/nndp_cpp/dpdl/VelControl.h\
    ../include/nndp_cpp/dpdl/JLTControl.h\
    ../include/nndp_cpp/dpdl/JLT.h

INCLUDEPATH += \
    ../include\
    ../../edt/include
