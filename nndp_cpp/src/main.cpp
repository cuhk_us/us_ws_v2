#include <iostream>
#include <nndp_cpp/optimization/Pso.h>
#include <chrono>
#include "edt/MapUpdater.h"
using namespace std;

int main(int argc, char *argv[])
{
    using namespace std::chrono;
    // Make sure the result is the same each time
    srand(1);
    cout << "Start" << endl;

    // Set up the map first
    LinDistMap dMap(100,100);
    PbtyMap pMap(100,100,20);

    DevGeo::pos origin;
    origin.x = -10;
    origin.y = -10;
    origin.z =  -1;
    dMap.setMapSpecs(origin,0.2);
    pMap.setMapSpecs(origin,0.2);

    // Add obstacles in to the pbty map
    DevGeo::coord c;
    for (int i=35;i<65;i++)
    {
        c.x = 50;
        c.y = i;
        c.z = 10;
        unsigned char *v=pMap.pbty(c);
        if (v)
            *v = 250;
    }

    // Do the linear distance map now
    MapUpdater mu(&dMap,&pMap,NULL);
    mu.updateEDTMap(-1, 3, DevGeo::pos(0,0,0));


    // Set up the data map
    // Initialize the Pso
    Dpdl::vec3d max_bound, min_bound;

#ifdef VEL_CONTROL
        max_bound.setData(20,20,1);
        min_bound.setData(-20,-20,-1);
        Pso o(40,20,5,0,&testMap,max_bound,min_bound);
#else
        max_bound.setData(10,10,1.8);
        min_bound.setData(-10,-10,0.3);
        Pso o(50,20,5,1,&dMap,max_bound,min_bound);
#endif

#ifdef VEL_CONTROL
        std::string str = "/home/sp/ctrlpred/vel_1";
#else
        std::string str = "/home/sp/ctrlpred/pos_1";
#endif
        PosPredictor::range r;
        r.glb_max = max_bound;
        r.glb_min = min_bound;
        r.loc_h=5;
        r.loc_v=1;
        o.addPredictor(str,4,4,r);

    // Setup initial state and goal position
    RefGenerator::state ini;
    ini.pos.setData(-5,-5,1);
    Dpdl::vec3d goal;
    goal.setData(7,7,1);

    // Setup the controller
    std::string name;
#ifdef VEL_CONTROL
    name = "/home/sp/ctrlpred/vel_1";
#else
    name = "/home/sp/ctrlpred/pos_1";
#endif
    RefGenerator rg;
    rg.addController(name);
    rg.setInternalState(ini);

    // Start the simulation
    Pso::TargetInfo tgt;
    Dpdl::vec3d pre_tgt = ini.pos;
    RefGenerator::state s ;

    std::ofstream myfile;
    std::vector<DevGeo::pos> trajLog;
    myfile.open("Trajectory.txt");
    for (int i=0;i<20;i++)
    {
        high_resolution_clock::time_point t1 = high_resolution_clock::now();
        tgt = o.selectTgt(ini,pre_tgt,goal);
        high_resolution_clock::time_point t2 = high_resolution_clock::now();

        duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

        std::cout << "It took me " << time_span.count() << " seconds.";
        std::cout << std::endl;
        pre_tgt = tgt.target;

        for (int j=0;j<10;j++)
        {
            s = rg.calculateNextCycleReference(tgt.target);
            myfile<<s.pos[0]<<" "<<s.pos[1]<<" "<<s.pos[2]<<" "<<
                              s.vel[0]<<" "<<s.vel[1]<<" "<<s.vel[2]<<" "<<
                              s.acc[0]<<" "<<s.acc[1]<<" "<<s.acc[2]<<
                           " "<<tgt.target[0]<<" "<<tgt.target[1]<<" "<<tgt.target[2]<<std::endl;
            DevGeo::pos ps;
            ps.x = s.pos[0];
            ps.y = s.pos[1];
            ps.z = s.pos[2];
            trajLog.push_back(ps);
        }

        ini = s;
        std::cout<<tgt.target[0]<<" "<<tgt.target[1]<<" "<<tgt.target[2]<<std::endl;
        std::cout<<tgt.collision<<std::endl;
    }
    myfile.close();

    return 0;
}
