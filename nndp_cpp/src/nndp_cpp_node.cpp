#include <iostream>
#include <nndp_cpp/optimization/Pso.h>
#include <chrono>
#include <edt/CostMap.h>
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PointStamped.h>
#include <common_msgs/state.h>
#include <common_msgs/target.h>
#include <std_msgs/Bool.h>
#include <chrono>
#include <tf/transform_broadcaster.h>

#define PUBLISH_PC
//#define HANDHELD
#define LOG_TGT

#ifdef PUBLISH_PC
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
PointCloud::Ptr pclOut (new PointCloud);
PointCloud::Ptr spOut (new PointCloud);
#endif

class NNDP
{
public:
    NNDP()
    {
        nh.param<std::string>("/home_name",home_name,"/home/nvidia");
        nh.param<double>("/nndp_cpp/fly_height",FLYHEIGHT,1.0);
        engaged = false;
    #ifdef PUBLISH_PC
        pclOut->header.frame_id = "/map";
        spOut->header.frame_id = "/map";
        pc_pub = nh.advertise<PointCloud> ("points2", 1);
        sp_pub = nh.advertise<PointCloud> ("points_sp", 1);
        tgt_show_pub = nh.advertise<geometry_msgs::PoseStamped>("/nndp_cpp/tgt_show",1);
    #endif

    #ifdef LOG_TGT
        myfile.open(home_name+"/Target.txt");
    #endif
        tgt_pub = nh.advertise<common_msgs::target>("/nndp_cpp/tgt",1);
        map_sub = nh.subscribe("/cost_map", 1, &NNDP::mapCallback, this);
        pos_cmd_sub = nh.subscribe("/dummy/pos_cmd", 1, &NNDP::posCmdCallback, this);
        engage_sub = nh.subscribe("/engage_cmd", 1, &NNDP::engageCallback, this);
        current_state_sub = nh.subscribe("/rt_ref_gen/current_state", 1, &NNDP::updateStateFromController, this);
        o_sub = nh.subscribe("/mavros/position/local", 1, &NNDP::updateStateFromMeasurement, this);

        planning_timer = nh.createTimer(ros::Duration(0.1), &NNDP::planCallback, this);
    }
    //---
    ~NNDP()
    {
        if (tMap != NULL)
        {
            delete tMap;
            delete optimizer;
        }

    #ifdef LOG_TGT
        myfile.close();
    #endif
    }
    //---
private:
    ros::NodeHandle nh;
    ros::Subscriber map_sub;
    ros::Subscriber pos_cmd_sub;
    ros::Subscriber engage_sub;
    ros::Subscriber current_state_sub;
    ros::Timer planning_timer;
#ifdef PUBLISH_PC
    ros::Publisher pc_pub;
    ros::Publisher sp_pub;
    ros::Publisher tgt_show_pub;
#endif

    LinDistMap* tMap=NULL;
    Pso* optimizer=NULL;
    DevGeo::pos origin;
    Pso::TargetInfo tgt;
    Dpdl::vec3d goal;
    ros::Publisher tgt_pub;
    ros::Subscriber o_sub;
    RefGenerator::state ini;
    bool engaged;
    std::string home_name;
    double FLYHEIGHT;

#ifdef LOG_TGT
    std::ofstream myfile;
#endif

    // **************************************************************
    // For the Pso::par_eval to access the map safely, the write to map
    // can only be in the main thread, because the main thread will be
    // blocked until all the read from map is finished.
    // **************************************************************
    void mapCallback(const edt::CostMap::ConstPtr& msg)
    {
        if (tMap == NULL)
        {
            tMap = new LinDistMap(msg->x_length,msg->y_length);
            origin = DevGeo::pos(msg->x_origin,msg->y_origin,msg->z_origin);
            tMap->setMapSpecs(origin,msg->res);
            //---
            Dpdl::vec3d max_bound, min_bound;
#ifdef VEL_CONTROL
            max_bound.setData(20,20,1);
            min_bound.setData(-20,-20,-1);
            optimizer = new Pso(40,20,5,0,tMap,max_bound,min_bound);
#else
            max_bound.setData(100,100,FLYHEIGHT);
            min_bound.setData(-100,-100,FLYHEIGHT);
            optimizer = new Pso(80,12,tMap);
#endif

#ifdef VEL_CONTROL
            std::string str = home_name+"/ctrlpred/vel_1";
#else
            std::string str = home_name+"/ctrlpred/pos_1";
#endif
            PosPredictor::range ran;
            ran.glb_max=max_bound;
            ran.glb_min=min_bound;
            ran.loc_h = 4;
            ran.loc_v = 1;
            optimizer->addPredictor(str,4,4,ran);
        }
        origin = DevGeo::pos(msg->x_origin,msg->y_origin,msg->z_origin);
        tMap->setOrigin(origin);
        tMap->copyData(msg->payload8.data(),msg->x_length,msg->y_length,origin,msg->res);

#ifdef PUBLISH_PC
        DevGeo::coord c;
        DevGeo::pos p;
        for (int x=0;x<msg->x_length;x++)
        {
            for (int y=0;y<msg->y_length;y++)
            {
                for (int z=0;z<msg->z_length;z++)
                {
                    c.x = x;
                    c.y = y;
                    c.z = z;
                    float d = tMap->distAt(c,0);
                    bool s = tMap->isSeen(c,0);
                    if (d < 4.1)
                    {
                        p = tMap->coord2pos(c);
                        pcl::PointXYZ clrP;
                        clrP.x = p.x;
                        clrP.y = p.y;
                        clrP.z = FLYHEIGHT;
                        pclOut->points.push_back (clrP);
                    }

                    if (s)
                    {
                        p = tMap->coord2pos(c);
                        pcl::PointXYZ clrP;
                        clrP.x = p.x;
                        clrP.y = p.y;
                        clrP.z = FLYHEIGHT-0.5;
                        spOut->points.push_back (clrP);
                    }

                }
            }
        }
        pcl_conversions::toPCL(ros::Time::now(), pclOut->header.stamp);
        pcl_conversions::toPCL(ros::Time::now(), spOut->header.stamp);
        pc_pub.publish (pclOut);
        sp_pub.publish(spOut);
        pclOut->clear();
        spOut->clear();
#endif
    }

    void posCmdCallback(const geometry_msgs::Point::ConstPtr& msg)
    {
        goal.a = msg->x;
        goal.b = msg->y;
        goal.c = msg->z;
    }

    void planCallback(const ros::TimerEvent&)
    {
        if (tMap == NULL)
            return;
        auto start = std::chrono::steady_clock::now();
        Dpdl::vec3d pre_tgt = tgt.target;
        optimizer->selectTgt(ini,pre_tgt,goal,tgt);
        auto end = std::chrono::steady_clock::now();
        std::cout << "NNDP time: "
                  << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
                  << " ms" << std::endl;

        if (tgt.collision)
        {
            std::cout<<"No solution!"<<std::endl;
        }

        // set the yaw target
        Dpdl::vec3d diff = goal-ini.pos;
        diff.c = 0;
        double dist = sqrt(diff.square());
        if (dist > 0.5)
        {
            tgt.yaw_target = atan2(diff.b,diff.a);
            tgt.yaw_target = tgt.yaw_target - ini.yaw.a;
            tgt.yaw_target = tgt.yaw_target - floor((tgt.yaw_target + M_PI) / (2 * M_PI)) * 2 * M_PI;
            tgt.yaw_target = tgt.yaw_target + ini.yaw.a;
        }

        common_msgs::target msg;
        msg.header.frame_id = "\map";
        msg.engaged=engaged;
        if (engaged)
        {
            msg.tgt.x = tgt.target.a;
            msg.tgt.y = tgt.target.b;
            msg.tgt.z = tgt.target.c;
            msg.yaw_tgt = tgt.yaw_target;
        }
        else
        {
            msg.tgt.x = ini.pos.a;
            msg.tgt.y = ini.pos.b;
            msg.tgt.z = ini.pos.c;
            msg.yaw_tgt = ini.yaw.a;
        }
        tgt_pub.publish(msg);
#ifdef PUBLISH_PC
        geometry_msgs::PoseStamped tgt_show;
        tgt_show.header.frame_id="\map";
        tgt_show.pose.position.x = msg.tgt.x;
        tgt_show.pose.position.y = msg.tgt.y;
        tgt_show.pose.position.z = msg.tgt.z;
        tf::Quaternion quat = tf::createQuaternionFromYaw(msg.yaw_tgt);
        tgt_show.pose.orientation.w = quat.w();
        tgt_show.pose.orientation.x = quat.x();
        tgt_show.pose.orientation.y = quat.y();
        tgt_show.pose.orientation.z = quat.z();
        tgt_show_pub.publish(tgt_show);
#endif

        std::cout<<"Target: "<<tgt.target[0]<<" "<<tgt.target[1]<<" "<<tgt.target[2]<<" "<<tgt.collision<<std::endl;
#ifdef LOG_TGT
        myfile<<tgt.target[0]<<" "<<tgt.target[1]<<" "<<tgt.target[2]<<" "<<tgt.collision<<" "<<ini.yaw.a<<" "<<tgt.yaw_target<<std::endl;
#endif
    }

    void updateStateFromController(const common_msgs::state::ConstPtr& msg)
    {
        if (!engaged)
            return;

        ini.pos.a = msg->pos.x;
        ini.pos.b = msg->pos.y;
        ini.pos.c = msg->pos.z;

        ini.vel.a = msg->vel.x;
        ini.vel.b = msg->vel.y;
        ini.vel.c = msg->vel.z;

        ini.acc.a = msg->acc.x;
        ini.acc.b = msg->acc.y;
        ini.acc.c = msg->acc.z;

        // use x for yaw, y for yaw speed and z for yaw acceleration
        ini.yaw.a = msg->yaw.x;
        ini.yaw.b = msg->yaw.y;
        ini.yaw.c = msg->yaw.z;
    }

    void updateStateFromMeasurement(const geometry_msgs::PoseStamped::ConstPtr &msg)
    {
        if (engaged)
            return;

        // calculate the angle
        double phi,theta,psi;
        tf::Quaternion q(msg->pose.orientation.x, msg->pose.orientation.y, msg->pose.orientation.z,msg->pose.orientation.w);
        tf::Matrix3x3 m(q);
        m.getRPY(phi, theta, psi);

        // update initial state
        ini.pos.setData(msg->pose.position.x,msg->pose.position.y,msg->pose.position.z);
        ini.vel.setData(0.0, 0.0, 0.0);
        ini.acc.setData(0.0, 0.0, 0.0);
        ini.yaw.setData(psi, 0.0, 0.0);

#ifndef HANDHELD
        // update goal
        goal = ini.pos;
        goal.c = FLYHEIGHT;
#endif

        // update the tgt
        tgt.target = ini.pos;
        tgt.yaw_target = ini.yaw.a;
    }

    void engageCallback(const std_msgs::Bool::ConstPtr &msg)
    {
        engaged = msg->data;
        if (engaged)
            o_sub.shutdown();
    }
};

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "nndp_cpp");
    NNDP planner;
    ros::spin();


    return 0;
}
