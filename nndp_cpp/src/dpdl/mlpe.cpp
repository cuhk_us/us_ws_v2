#include <iostream>
#include <string>
#include <nndp_cpp/dpdl/mlpe.h>

//Load the mlp matrixes
Mlpe::Mlpe(std::string arrs[], int layer_num):
    _layer_num(layer_num)
{
    cnpy::NpyArray arrpy[2];

    _W = new Eigen::MatrixXf[layer_num];
    _b = new Eigen::MatrixXf[layer_num];
    _x = new Eigen::VectorXf[layer_num+1];

    for (int i=0;i<_layer_num;i++)
    {
        for (int j=0;j<2;j++)
        {
            arrpy[j] = cnpy::npy_load(arrs[i*2+j]);
        }
        _W[i] = loadEigenMatrix(arrpy[0]);
        _b[i] = loadEigenMatrix(arrpy[1]);
        _x[i+1] = Eigen::VectorXf(_b[i].rows());
        arrpy[0].destruct();
        arrpy[1].destruct();
    }

    _x[0] = Eigen::VectorXf(_W[0].cols());
    _input_size = _x[0].rows();
    _output_size = _x[_layer_num].rows();
}

Mlpe::~Mlpe()
{
    delete [] _W;
    delete [] _b;
    delete [] _x;
}

// run the feedforward network
Mlpe::nRes Mlpe::evaluate(const Dpdl::vec3d &pva, const double &ptgt, const int &effective_point_size)
{
    std::lock_guard<std::mutex> guard(_lock);
    //input layter
    //----------------------------------
    for (int i=0;i<_x[0].rows();i++)
    {
        if (i == 0)
            _x[0](i)=pva.const_at(i) - ptgt; // position shift
        else
            _x[0](i)=pva.const_at(i); // others no need to shift
    }

    //hidden layer
    //----------------------------------
    for (int l=1;l<_layer_num;l++)
    {
        _x[l] = _W[l-1]*_x[l-1] + _b[l-1];

        for (int k=0;k<_x[l].rows();k++)
        {
            if(_x[l](k)<0)
            {
                _x[l](k)=0;
            }
        }
    }

    //output
    //-----------------------------------
    _x[_layer_num] = _W[_layer_num-1]*_x[_layer_num-1]+_b[_layer_num-1];

    //copy to result
    //----------------------------------
    Mlpe::nRes result;
    result.pList.push_back(pva.const_at(0));
    for (int i=0;i<effective_point_size;i++)
    {
        result.pList.push_back(_x[_layer_num](i)+ptgt);
    }

    // Setup the u_square
    result.u_sq = _x[_layer_num](_output_size-1);

    // Record the target
    result.tgt = ptgt;

    return result;
}

Eigen::MatrixXf Mlpe::loadEigenMatrix(cnpy::NpyArray arr)
{
    int colct;
    if(arr.shape.size() < 2)
    {
        colct=1;
    }
    else
    {
        colct=arr.shape[1];
    }
    std::cout<<arr.shape[0]<<" "<<colct<<std::endl;
    Eigen::MatrixXf m(arr.shape[0],colct);
    float* data = reinterpret_cast<float*>(arr.data);
    for (int i=0; i<m.rows(); i++)
    {
        for (int j=0; j<m.cols(); j++)
        {
            m(i,j) = data[i*m.cols()+j];
            //std::cout<<m(i,j)<<", ";
        }
        //std::cout<<std::endl;
    }
    return m;
}


