#include "nndp_cpp/dpdl/RefGenerator.h"

RefGenerator::RefGenerator(std::string yaw_ctrl_str)
{
    _yaw_ctrl = new JLTControl(yaw_ctrl_str);
}

void RefGenerator::addController(const std::string &name)
{
    std::string hor,ver;
    extendName(name, hor, ver);
    if (checkName(name,"pos"))
    {
        _hor_ctrls.push_back(new PosControl(hor));
        _ver_ctrls.push_back(new PosControl(ver));
    }
    else if (checkName(name,"vel"))
    {
        _hor_ctrls.push_back(new VelControl(hor));
        _ver_ctrls.push_back(new VelControl(ver));
    }
    else if (checkName(name,"jlt"))
    {
        _hor_ctrls.push_back(new JLTControl(hor));
        _ver_ctrls.push_back(new JLTControl(ver));
    }
    else
    {
        assert(0 && "Loaded an unknown controller.\n");
    }

    //Exam the controllers
    //1. There must be both horizon and vertial controllers
    assert(_hor_ctrls.size()>0 && "There must be at least one horizontal controller.");
    assert(_ver_ctrls.size()>0 && "There must be at least one vertical controller.");

    //2. The dt for all controllers must be the same
    _dt = _hor_ctrls[0]->getDt();
    for (uint i= 0; i<_hor_ctrls.size(); i++)
    {
        assert(fabs(_hor_ctrls[i]->getDt() -_dt)<1e-6 && "The dt for all controllers must be the same.");
    }
    for (uint i= 0; i<_ver_ctrls.size(); i++)
    {
        assert(fabs(_ver_ctrls[i]->getDt() -_dt)<1e-6 && "The dt for all controllers must be the same.");
    }
}

bool RefGenerator::checkName(const std::string &name, const std::string &str)
{
    if (name.find(str) != std::string::npos)
        return true;
    else
        return false;
}

void RefGenerator::extendName(const std::string &name, std::string &hor, std::string &ver)
{
    hor = name + "/hor/ctrl";
    ver = name + "/ver/ctrl";
}

RefGenerator::~RefGenerator()
{
    // Delete all loaded controllers
    for (uint i= 0; i<_hor_ctrls.size(); i++)
    {
        delete _hor_ctrls[i];
    }

    for (uint i= 0; i<_ver_ctrls.size(); i++)
    {
        delete _ver_ctrls[i];
    }

    delete _yaw_ctrl;
}

void RefGenerator::setInternalState(const state &s)
{
    _s = s;
}

RefGenerator::state RefGenerator::calculateNextCycleReference(const common_msgs::target &tgt)
{

    progress(_s.pos[0],_s.vel[0],_s.acc[0],tgt.tgt.x,_hor_ctrls[0]);
    progress(_s.pos[1],_s.vel[1],_s.acc[1],tgt.tgt.y,_hor_ctrls[0]);
    progress(_s.pos[2],_s.vel[2],_s.acc[2],tgt.tgt.z,_ver_ctrls[0]);
    progress(_s.yaw[0],_s.yaw[1],_s.yaw[2],tgt.yaw_tgt,_yaw_ctrl);
    return _s;
}

void RefGenerator::progress(double& p, double& v, double& a, const double& target, Controller* ctrl)
{
    Dpdl::vec3d cur_state;
    cur_state.setData(p,v,a);
    Dpdl::vec3d next_state = ctrl->act(cur_state,target);
    p = next_state[0];
    v = next_state[1];
    a = next_state[2];
}
