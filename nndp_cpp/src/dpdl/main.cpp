#include <iostream>
#include <fstream>
#include <string.h>
#include <MatrixLoader.h>
#include <RefGenerator.h>
#include <mlpe.h>
#include <chrono>

double randMToN(double M, double N)
{
    return M + (rand() / ( RAND_MAX / (N-M) ) ) ;
}

int main()
{
        Controller ctrl_1("ctrl_5/hor");
        Dpdl::vec3d state;
        state.setData(100.5232,40.33,102.1234);
        std::cout<<ctrl_1.act(state)<<std::endl;

        std::vector<std::string> names_hor;
        std::vector<std::string> names_ver;
        names_hor.push_back("ctrl_5/hor");
        names_ver.push_back("ctrl_5/ver");
        RefGenerator rg(names_hor, names_ver);
        RefGenerator::state ini;
        ini.pos[0]=10;
        ini.vel[0]=0;
        ini.acc[0]=0;
        rg.setInternalState(ini);
        std::ofstream myfile;
        myfile.open("Trajectory.txt");
        int i=1;
        double xx = -100;
        double randNum[3]={0,0,0};
        Dpdl::vec3d tgt;
        for (double t=0;t<10;t+=0.05)
        {
            if (i%10==0)
            {
                xx = -xx;
                i = 1;
                randNum[0] = 0*randMToN(-1,1);
                randNum[1] = 0*randMToN(-1,1);
                randNum[2] = 0*randMToN(-1,1);
            }
            else
            {
                i++;
            }
            RefGenerator::state s = rg.calculateNextCycleReference(tgt);
            myfile<<s.pos[0]<<" "<<s.pos[1]<<" "<<s.pos[2]<<" "<<
                              s.vel[0]<<" "<<s.vel[1]<<" "<<s.vel[2]<<" "<<
                              s.acc[0]<<" "<<s.acc[1]<<" "<<s.acc[2]<<std::endl;
        }
        myfile.close();

//    std::string names[8];
//    names[0] = "ctrl_5/hor_nn/array_0.npy";
//    names[1] = "ctrl_5/hor_nn/array_1.npy";
//    names[2] = "ctrl_5/hor_nn/array_2.npy";
//    names[3] = "ctrl_5/hor_nn/array_3.npy";
//    names[4] = "ctrl_5/hor_nn/array_4.npy";
//    names[5] = "ctrl_5/hor_nn/array_5.npy";
//    names[6] = "ctrl_5/hor_nn/array_6.npy";
//    names[7] = "ctrl_5/hor_nn/array_7.npy";
//    Mlpe nn(names, 4);
//    float in[3],out[41];

//    auto start = std::chrono::high_resolution_clock::now();
//    for (int i=0;i<1000;i++)
//    {
//        in[0] = 3;in[1]=1;in[2]=1;
//        nn.evaluate(in,out);
//    }
//    auto finish = std::chrono::high_resolution_clock::now();
//    std::chrono::duration<double> elapsed = finish - start;
//    std::cout << "Elapsed time: " << elapsed.count() << " s\n";

    return 0;
}
