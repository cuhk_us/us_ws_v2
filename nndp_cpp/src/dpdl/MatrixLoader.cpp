#include "nndp_cpp/dpdl/MatrixLoader.h"

MatrixLoader::MatrixLoader()
{
    assert(sizeof(double) == 8 && "The size of double must be 8 to use the code.");
}

char* MatrixLoader::readFileBytes(const char *name, size_t& len)
{
    std::ifstream fl(name);
    fl.seekg( 0, std::ios::end );
    len = fl.tellg();
    char *ret = new char[len];
    fl.seekg(0, std::ios::beg);
    fl.read(ret, len);
    fl.close();
    return ret;
}

Eigen::MatrixXd MatrixLoader::loadMatrix(std::string name)
{
    std::cout<<"Loading from: "<<name<<std::endl;

    // Read off the bytes and check whether the num of bytes is corrected
    size_t len = 0;
    char* ret = readFileBytes(name.c_str(), len);
    assert(len%sizeof(double) == 0 && "The file might be broken. Its length is not 8*N.");

    // Copy to the double array and delete the bytes array
    size_t len_d = len/sizeof(double);
    double data[len_d];

    memcpy(data, ret, sizeof(double)*len_d);
    delete[] ret;

    //The last two number of the array is the row and col number
    uint rowSize = data[len_d-2];
    uint colSize = data[len_d-1];
    assert(rowSize*colSize == len_d-2 && "The file might be broken. It sides does not match the rowSize*colSize description.");

    //Copy data to the matrix
    Eigen::MatrixXd mat(rowSize,colSize);
    for (uint i=0;i<colSize;i++)
    {
        for (uint j=0;j<rowSize;j++)
        {
            mat(j,i) = data[i*rowSize+j];
        }
    }
    return mat;
}
