TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp\
        cnpy.cpp \
    MatrixLoader.cpp \
    Controller.cpp \
    RefGenerator.cpp \
    mlpe.cpp

HEADERS += cnpy.h \
    MatrixLoader.h \
    Controller.h \
    RefGenerator.h \
    mlpe.h

INCLUDEPATH += \
    ../include\
    ../include/Eigen\
    ../include/unsupported
