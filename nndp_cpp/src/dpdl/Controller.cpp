#include "nndp_cpp/dpdl/Controller.h"

Controller::Controller()
{

}

Controller::~Controller()
{

}

void Controller::bound(double &in, double min, double max)
{
    if (in < min)
    {
        in = min;
    }
    if (in > max)
    {
        in = max;
    }
}

int Controller::searchIndex(double val, const Eigen::MatrixXd &bins)
{
    int N = bins.cols() - 1;
    int left = 0;
    int right = N;
    int mid = 0;
    int idx = 0;
    if (val >= bins(0,N))
    {
        idx = N-1;
    }
    else
    {
        while(1)
        {
            mid = (left+right)/2;
            if (val >= bins(0,mid))
            {
                left = mid;
            }
            else
            {
                right = mid;
            }

            if (right - left == 1)
            {
                idx = left;
                break;
            }
        }
    }

    return idx;
}
