#include "nndp_cpp/dpdl/VelControl.h"

VelControl::VelControl()
{

}

VelControl::VelControl(std::string name)
{
    //Load the parameters
    Eigen::MatrixXd dtTemp = _loader.loadMatrix(name+"/"+"dt"+".bin");
    _dt = dtTemp(0,0);
    _PI = _loader.loadMatrix(name+"/"+"PI"+".bin");
    _q_bins = _loader.loadMatrix(name+"/"+"q_bins"+".bin");
    _qdot_bins = _loader.loadMatrix(name+"/"+"qdot_bins"+".bin");
}

Dpdl::vec3d VelControl::act(Dpdl::vec3d cur, double tgt)
{
    Dpdl::vec3d output = cur;
    //cur[0],cur[1],cur[2] -> p, v, a
    Dpdl::vec2d s; //related state, s[0],s[1]->v, a
    s[0] = cur[1] - tgt;
    s[1] = cur[2];

    //Force s in the mapping area
    bound(s[0],_q_bins(0,0),_q_bins(0,_q_bins.cols()-1));
    bound(s[1],_qdot_bins(0,0),_qdot_bins(0,_qdot_bins.cols()-1));

    //Search for the index on each p, v, a axis
    int ind_q = searchIndex(s[0],_q_bins);
    int ind_qdot = searchIndex(s[1],_qdot_bins);

    //The volume for this grid
    double volume = (_q_bins(0,ind_q+1)-_q_bins(0,ind_q))*
            (_qdot_bins(0,ind_qdot+1)-_qdot_bins(0,ind_qdot));

    point p,op;
    Dpdl::vec2d val;
    int Pi_ind;
    double weight;
    double u = 0;
    for (int i = 0; i <= 1; i++)
    {
        for (int j = 0; j <= 1; j++)
        {
            p.loc.setData(i,j);
            p.glb.setData(i+ind_q,j+ind_qdot);
            op = getOppositePoint(p);
            val.setData(_q_bins(0, op.glb[0]),_qdot_bins(0, op.glb[1]));
            weight = fabs(s[0]-val[0])*fabs(s[1]-val[1])/volume;
            Pi_ind = p.glb[1]*_q_bins.cols()+
                     p.glb[0];
            u += _PI(Pi_ind,0)*weight;
        }
    }
    simulate(output[0],output[1],output[2],u);
    return output;
}

void VelControl::simulate(double& p, double& v, double& a, const double& u)
{
    // Sequence matters here
    p = p + v*_dt + a*_dt*_dt/2.0 + u*_dt*_dt*_dt/6.0;
    v = v + a*_dt + u*_dt*_dt/2.0;
    a = a + u*_dt;
}

VelControl::point VelControl::getOppositePoint(const point & p)
{
    Dpdl::vec2i ind = p.glb - p.loc;
    point oppoPoint;
    for (int i=0;i<2;i++)
    {
        if (p.loc.const_at(i) == 0)
        {
            oppoPoint.loc[i] = 1;
        }
        else
        {
            oppoPoint.loc[i] = 0;
        }
    }
    oppoPoint.glb = oppoPoint.loc + ind;

    return oppoPoint;
}

double VelControl::getDt()
{
  return _dt;
}
