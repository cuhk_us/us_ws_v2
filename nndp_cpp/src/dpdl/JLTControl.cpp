#include "nndp_cpp/dpdl/JLTControl.h"
JLTControl::JLTControl(std::string name)
{
    name.append("/JLTSetting.txt");
    JLT::loadTimeAndLimFromFile(name,_dt,_lim);
}

Dpdl::vec3d JLTControl::act(Dpdl::vec3d s, double tgt)
{
    // Declare the output
    Dpdl::vec3d output;

    // Set the initial state
    _ini.p = s[0];
    _ini.v = s[1];
    _ini.a = s[2];

    // Calculate the next state reference
    bool ok = _gen.solveTPBVP(tgt,0,_ini,_lim,_sp);
    if(!ok)
    {
        assert(0);
    }
    _next = _gen.TPBVPRefGen(_sp,_dt);

    // Setup the value of the output
    output[0] = _next.p;
    output[1] = _next.v;
    output[2] = _next.a;

    return output;
}

double JLTControl::getDt()
{
  return _dt;
}
