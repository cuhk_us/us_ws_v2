#include "edt/LinDistMap.h"
#include <string.h>
LinDistMap::LinDistMap(int maxX, int maxY):
    MapBase(),
    _w(maxX),
    _h(maxY)
{
    _d_h = new float[_h];
    _d_w = new float[_w];
    _v_h = new int[_h];
    _v_w = new int[_w];
    _z_h = new float[_h+1];
    _z_w = new float[_w+1];
    _f = new float[std::max(_w,_h)];

    _map = new SeenDist[_w*_h];
    memset(_map, 0, _w*_h*sizeof(SeenDist));
}

LinDistMap::~LinDistMap()
{
    delete [] _d_h;
    delete [] _d_w;
    delete [] _v_h;
    delete [] _v_w;
    delete [] _z_h;
    delete [] _z_w;
    delete [] _f;
    delete [] _map;
}

float LinDistMap::distAt(const DevGeo::coord & s, const float default_value) const
{
    if (s.x<0 || s.x>=_w ||
            s.y<0 || s.y>=_h)
        return default_value;

    return _map[s.y*_w+s.x].d;
}

bool LinDistMap::isSeen(const DevGeo::coord & s, const bool default_value) const
{
    if (s.x<0 || s.x>=_w ||
            s.y<0 || s.y>=_h)
        return default_value;

    return _map[s.y*_w+s.x].s;
}

/* dt of 1d function using squared distance */
float* LinDistMap::dt(float *f, int n)
{
    float *d, *z;
    int *v;
    if (n == _w)
    {
        d = _d_w;
        v = _v_w;
        z = _z_w;
    }
    else if (n == _h)
    {
        d = _d_h;
        v = _v_h;
        z = _z_h;
    }
    else
    {
        assert(0 && "Mismatch in size, one dimensional dt.");
    }

    int k = 0;
    v[0] = 0;
    z[0] = -INF;
    z[1] = +INF;
    for (int q = 1; q <= n-1; q++)
    {
        float s  = ((f[q]+square(q))-(f[v[k]]+square(v[k])))/(2*q-2*v[k]);
        while (s <= z[k])
        {
            k--;
            s  = ((f[q]+square(q))-(f[v[k]]+square(v[k])))/(2*q-2*v[k]);
        }
        k++;
        v[k] = q;
        z[k] = s;
        z[k+1] = +INF;
    }


    k = 0;
    for (int q = 0; q <= n-1; q++)
    {
        while (z[k+1] < q)
            k++;
        d[q] = square(q-v[k]) + f[v[k]];
    }
    return d;
}

/* dt of 2d function using squared distance */
void LinDistMap::edt()
{
    float *d;
    // transform along columns
    for (int x = 0; x < _w; x++)
    {
        for (int y = 0; y < _h; y++)
        {
            _f[y] = _map[y*_w+x].d;
        }
        d = dt(_f, _h);
        for (int y = 0; y < _h; y++)
        {
            _map[y*_w+x].d = d[y];
        }
    }

    // transform along rows
    for (int y = 0; y < _h; y++)
    {
        for (int x = 0; x < _w; x++)
        {
            _f[x] = _map[y*_w+x].d;
        }
        d = dt(_f, _w);
        for (int x = 0; x < _w; x++)
        {
            _map[y*_w+x].d = d[x];
        }
    }

    //take sqrt
    for (int y = 0; y < _h; y++)
    {
        for (int x = 0; x < _w; x++)
        {
            _map[y*_w+x].d = sqrt(_map[y*_w+x].d);
        }
    }
}

void LinDistMap::updateFromPbtyMap(PbtyMap *pMap, int minZ, int maxZ, const DevGeo::coord &shift)
{
    DevGeo::coord s;
    unsigned char* pbValue;
    bool occupied = false;
    for (s.y = 0; s.y < _h; s.y++)
    {
        for (s.x = 0; s.x < _w; s.x++)
        {
            occupied = false;
            for (s.z = minZ; s.z<=maxZ; s.z++)
            {
                pbValue = pMap->pbty(s+shift);
                if (pbValue && *pbValue > EDTMap::OCCUPY_TH)
                {
                    occupied = true;
                    break;
                }
            }
            if (occupied)
                _map[s.y*_w+s.x].d = 0;
            else
                _map[s.y*_w+s.x].d = INF;
        }
    }
}

void LinDistMap::copyData(const unsigned char* data, int maxX, int maxY,
                        const DevGeo::pos &origin, const double &gridstep)
{
    if (maxX != _w || maxY != _h)
    {
        printf("Dimension mismatch during map data copying!\n Map is not copied!\n");
        return;
    }
    if (!(origin == _origin))
    {
        printf("Origin mismatched!\n Map is not copied!\n");
        return;
    }
    if (gridstep != _gridstep)
    {
        printf("Gridstep mismatched!\n Map is not copied!\n");
        return;
    }
    memcpy (_map, data, sizeof(SeenDist)*_w*_h);
}

void LinDistMap::evaTrajectory(const std::list<DevGeo::pos> &posList, const double &rClose,
                                const double &rVehicle, double &cost, bool &collision, const double &curr_yaw) const
{
    SeenDist seg_min;
    float real_min_dist;

    if(posList.size()>1) //Only start processing when there are at leat two points (one line-segment)
    {
        std::list<DevGeo::pos>::const_iterator ptr1;
        std::list<DevGeo::pos>::const_iterator ptr2 = posList.begin();
        DevGeo::pos drone = *ptr2;
        double theta;
        DevGeo::pos diff;
        for (unsigned int i=0;i<posList.size()-1;i++)
        {
            // Increase the iterator
            ptr1 = ptr2;
            ptr2 = std::next(ptr1);

            // Insert to output
            seg_min = minDistOnLineSeg(*ptr1, *ptr2);
            real_min_dist = seg_min.d*getGridStep();

            // update the distance cost, if the min_dist is smaller than the free distance
            if (seg_min.d < (float)EDTMap::MAX_RANGE - 0.1)
                cost += exp(-6*real_min_dist)*400;

            if (real_min_dist <= rClose)
                cost += 20;

            if (!seg_min.s)
                cost += 10;

            // check for bottom line collision condition
            if (real_min_dist < rVehicle)
                collision = true;

            // calculate whether *ptr2 point is inside the field of view
            diff = *ptr2 - drone;
            diff.z = 0;
            if (sqrt(diff.square()) > 0.3)
            {
                theta = atan2(diff.y,diff.x);
                theta -= curr_yaw;
                theta = theta - floor((theta + M_PI) / (2 * M_PI)) * 2 * M_PI;
                if (fabs(theta) > M_PI*0.25)
                    cost += 10;
            }
        }
    }
}

SeenDist LinDistMap::minDistOnLineSeg(const DevGeo::pos &p0, const DevGeo::pos &p1) const
{
    SeenDist seg_min;
    seg_min.d = (float)EDTMap::MAX_RANGE;
    seg_min.s = true;

    DevGeo::coord p0Index = pos2coord(p0);
    DevGeo::coord p1Index = pos2coord(p1);

    seg_min.d = std::min(seg_min.d,distAt(p0Index,(float)EDTMap::MAX_RANGE));
    seg_min.s = (seg_min.s && isSeen(p0Index,false));
    // same grid, we are done
    if(p0Index.x == p1Index.x && p0Index.y == p1Index.y && p0Index.z == p1Index.z)
    {
        return seg_min;
    }
    // Initialization phase ------------------------
    DevGeo::pos direction = p1 - p0;
    double length = sqrt(direction.square());
    direction = direction / length; //normalize the vector

    int    step[3];
    double tMax[3];
    double tDelta[3];

    DevGeo::coord currIndex = p0Index;
    for (unsigned int i=0; i<3; i++)
    {
        // compute step direction
        if(direction.at(i) > 0.0) step[i] = 1;
        else if (direction.at(i) < 0.0) step[i] = -1;
        else step[i] = 0;

        // compute tMax, tDelta
        //tMax: shortest route? tDelta?  voxelBorder=pos.x?  pos: /m   coord: idx of grid
        if(step[i] != 0)
        {
            double voxelBorder = double(currIndex.at(i)) * _gridstep +
                    _origin.const_at(i) + double(step[i]) * _gridstep*0.5;
            tMax[i] = (voxelBorder - p0.const_at(i))/direction.at(i);
            tDelta[i] = _gridstep / fabs(direction.at(i));
        }
        else
        {
            tMax[i] =  std::numeric_limits<double>::max( );
            tDelta[i] = std::numeric_limits<double>::max( );
        }
    }

    // Incremental phase -----------------------------------
    bool done = false;
    while (!done)
    {
        unsigned int dim;

        // find minimum tMax;
        if (tMax[0] < tMax[1]){
            if (tMax[0] < tMax[2]) dim = 0;
            else                   dim = 2;
        }
        else {
            if (tMax[1] < tMax[2]) dim = 1;
            else                   dim = 2;
        }

        // advance in drection "dim"
        currIndex.at(dim) += step[dim];
        tMax[dim] += tDelta[dim];
        seg_min.d = std::min(seg_min.d,distAt(currIndex,(float)EDTMap::MAX_RANGE));
        seg_min.s = (seg_min.s && isSeen(currIndex,false));

        // reached endpoint?
        if(currIndex.x == p1Index.x && currIndex.y == p1Index.y && currIndex.z == p1Index.z)
        {
            done = true;
            break;
        }
        else
        {
            double dist_from_origin = std::min(std::min(tMax[0], tMax[1]), tMax[2]);

            if(dist_from_origin > length)
            {
                done = true;
                break;
            }
        }
    }
    return seg_min;
}
