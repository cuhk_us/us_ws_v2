#include "edt/PbtyMap.h"
#include <iostream>
#include <string.h>
PbtyMap::PbtyMap(int maxX, int maxY, int maxZ):
    MapBase(),
    _maxX(maxX),
    _maxY(maxY),
    _maxZ(maxZ)
{
    _map = new unsigned char[_maxX*_maxY*_maxZ];
    _mapByteSize = _maxX*_maxY*_maxZ*sizeof(unsigned char);
    memset(_map, 0, _mapByteSize);
}

PbtyMap::~PbtyMap()
{
    std::cout<<"delete promap"<<std::endl;
    delete[] _map;
}

unsigned char* PbtyMap::pbty(const DevGeo::coord &s)
{
    if (s.x<0 || s.x>=_maxX ||
            s.y<0 || s.y>=_maxY ||
            s.z<0 || s.z>=_maxZ)
        return nullptr;

    return &_map[s.z*_maxX*_maxY+s.y*_maxX+s.x];
}
