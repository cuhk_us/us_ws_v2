#include "edt/lidar.h"
#include "edt/LidarKernelWrapper.h"

LidarUpdater::LidarUpdater(LinDistMap *dmap, DevMap *dev_map, const sensor_msgs::LaserScan::ConstPtr &msg):
    MapUpdater(dmap,dev_map)
{

    _lp.scan_num = msg->ranges.size();
    _lp.max_r = 10;
    _lp.theta_inc = msg->angle_increment;
    _lp.theta_min = msg->angle_min;

    _scan_sz = sizeof(float)* _lp.scan_num;
    CUDA_ALLOC_DEV_MEM(&_D_scan,_scan_sz);
}

LidarUpdater::~LidarUpdater()
{
    CUDA_FREE_DEV_MEM(_D_scan);
}

void LidarUpdater::copyScan(const sensor_msgs::LaserScan::ConstPtr &scan)
{
    float* H_scan=(float*)(&scan->ranges.at(0));
    CUDA_MEMCPY_H2D(_D_scan,H_scan,_scan_sz);
}

void LidarUpdater::makeLaserPt(const tf::Transform &trans,const sensor_msgs::LaserScan::ConstPtr &scan)
{
    updateProjection(trans);
    copyScan(scan);
    lidar::lidarKernelWrapper(_D_scan,_dev_map,_mp,_lp);
}
