#include "edt/MapBase.h"
#include <iostream>
MapBase::MapBase()
{
    // Initialize the origin and grid step
    _origin.x = 0;
    _origin.y = 0;
    _origin.z = 0;
    _gridstep = 0.2;
}

MapBase::~MapBase()
{
    std::cout<<"delete mapbase"<<std::endl;
}

DevGeo::coord MapBase::pos2coord(const DevGeo::pos & p) const
{
    DevGeo::coord output;
    output.x = floor( (p.x - _origin.x) / _gridstep + 0.5);
    output.y = floor( (p.y - _origin.y) / _gridstep + 0.5);
    output.z = floor( (p.z - _origin.z) / _gridstep + 0.5);
    return output;
}

DevGeo::pos MapBase::coord2pos(const DevGeo::coord & c) const
{
    DevGeo::pos output;
    output.x = c.x * _gridstep + _origin.x;
    output.y = c.y * _gridstep + _origin.y;
    output.z = c.z * _gridstep + _origin.z;

    return output;
}

void MapBase::setMapSpecs(const DevGeo::pos & origin, const double & gridstep)
{
    _origin = origin;
    _gridstep = gridstep;
}

void MapBase::setOrigin(const DevGeo::pos & origin)
{
    _origin = origin;
}

DevGeo::pos MapBase::getOrigin() const
{
    return _origin;
}

double MapBase::getGridStep() const
{
    return _gridstep;
}
