#include "edt/stereo.h"
#include <iostream>
#include <edt/timer.h>
#include "std_msgs/Float32.h"
#include "cuda_toolkit/cuda_exception.cuh"

void StereoUpdater::reAllocMem(int newRows, int newCols)
{
    _sp.cols=newCols;
    _sp.rows=newRows;
    freeMem();
    allocMem(newRows,newCols);
}
//===
void StereoUpdater::allocMem(int rows , int cols)
{
    // device: Depth image
    _depth_size=rows*cols*sizeof(float);
    CUDA_ALLOC_DEV_MEM(&_D_depth,_depth_size);

    // device: confidence map
    _confi_map_size=rows*cols*sizeof(float);
    CUDA_ALLOC_DEV_MEM(&_D_confi_map,_confi_map_size);
}
//===
void StereoUpdater::topic2Dmem(const sensor_msgs::Image::ConstPtr &depthPoint_ptr,
                               const sensor_msgs::Image::ConstPtr &confidence_ptr)
{
    float* depths = (float*)(&depthPoint_ptr->data[0]);
    CUDA_MEMCPY_H2D(_D_depth,depths,_depth_size);

//    float* confi_map = (float*)(&confidence_ptr->data[0]);
//    CUDA_MEMCPY_H2D(d_confi_map,confi_map,confi_map_size);
}
//===
void StereoUpdater::freeMem()
{
    CUDA_FREE_DEV_MEM(_D_depth);
    CUDA_FREE_DEV_MEM(_D_confi_map);
}
//===
StereoUpdater::StereoUpdater(LinDistMap *dmap, DevMap *dev_map,
                             StereoParams p):
    MapUpdater(dmap,dev_map),
    _sp(p)
{
    allocMem(_sp.rows,_sp.cols);
}
//===
StereoUpdater::~StereoUpdater()
{
    freeMem();
}
//===
void StereoUpdater::makeStereoPt(const tf::Transform &trans,
                                 const sensor_msgs::Image::ConstPtr &depthPoint,
                                 const sensor_msgs::Image::ConstPtr &confidence_ptr)
{
    // Get the current camera pose
    updateProjection(trans);

    // Copty the depthmap, etc into the device
    topic2Dmem(depthPoint,confidence_ptr);

    // Start the GPU kernel to construct the probability map
    stereo::stereoKernelWrapper(_sp,_mp,_D_depth,_D_confi_map,_dev_map);
}

