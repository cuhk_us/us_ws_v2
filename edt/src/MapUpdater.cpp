#include "edt/MapUpdater.h"
#include <math.h>
#include <edt/EdtPrepareKernelWrapper.h>
//#include "edt/timer.h"
//#include "std_msgs/Float32.h"

MapUpdater::MapUpdater(LinDistMap *dmap, DevMap *dev_map)
{
    _d_map = dmap;
    _dev_map = dev_map;
}
//---
cudaMat::SE3<float> MapUpdater::getCamPos(const tf::Transform &trans) const
{
    tf::Quaternion Rotq=trans.getRotation();
    tf::Vector3 transVec=trans.getOrigin();
    cudaMat::SE3<float> camPos(Rotq.w(),Rotq.x(),Rotq.y(),Rotq.z(),
                               transVec.m_floats[0],transVec.m_floats[1],transVec.m_floats[2]);
    return camPos;
}
//---
void MapUpdater::updateProjection(const tf::Transform &trans)
{
    _mp.camPos=getCamPos(trans);
    _mp.camPoseInv = _mp.camPos.inv();
    _mp.center.x = trans.getOrigin().x();
    _mp.center.y = trans.getOrigin().y();
    _mp.center.z = trans.getOrigin().z();
}
//---
void MapUpdater::updateEDTMap(const double &min_z_pos, const double &max_z_pos, const DevGeo::pos &center)
{
    // Get the vehicle's global coordinates
    DevGeo::coord c = _dev_map->pos2coord(center);

    // calculate the coord in the pbty that is mapped
    // to the (0,0) coord in the dist map
    c.x -= _d_map->getMaxX()/2;
    c.y -= _d_map->getMaxY()/2;
    c.z = 0; // set the z value as zero, because it is latter also used as the shift term in updateFromPbtyMap()

    // Update the distance map's origin to make the vehicle stay at the center grid
    _d_map->setOrigin(_dev_map->coord2pos(c));

    int max_z_coord = floor( (max_z_pos - _dev_map->origin.z) / _dev_map->gridstep + 0.5);
    int min_z_coord = floor( (min_z_pos - _dev_map->origin.z) / _dev_map->gridstep + 0.5);

    EdtPrepare::EdtPrepareKernelWrapper(_dev_map, c, max_z_coord, min_z_coord);
    CUDA_MEMCPY_D2H(_d_map->getMapPtr(),_dev_map->DEV_buf,_dev_map->bufByteSize);

    _d_map->edt();
}
