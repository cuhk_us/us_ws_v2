#include <ros/ros.h>
#include <edt/PbtyMap.h>
#include <edt/LinDistMap.h>
#include <edt/MapUpdater.h>
#include <sensor_msgs/PointCloud2.h>
#include <edt/CostMap.h>
#include <geometry_msgs/PoseStamped.h>
#include <gazebo_msgs/GetModelState.h>
#include <geometry_msgs/TransformStamped.h>

MapUpdater* mu = NULL;
ros::Publisher pub;
edt::CostMap msg;
LinDistMap dMap(100,100);
PbtyMap pMap(100,100,20);
boost::shared_ptr<geometry_msgs::PoseStamped>  pose_ptr;
DevGeo::pos obs_pos[16];
bool got_pose;

void updatePillar(const DevGeo::pos & p, const unsigned char occupancy)
{
    DevGeo::pos tmp_p = p;
    for (double z=0;z<=1.5;z+=0.2)
    {
        tmp_p.z = z;
        mu->updateGrid(tmp_p,occupancy);
    }
}

void publishMap(const ros::TimerEvent&)
{
    if (!got_pose)
        return;

    obs_pos[0].x = 1.25;
    obs_pos[0].y = 1.25;

    obs_pos[1].x = 1.25;
    obs_pos[1].y = -1.25;

    obs_pos[2].x = -1.25;
    obs_pos[2].y = 1.25;

    obs_pos[3].x = -1.25;
    obs_pos[3].y = -1.25;
    updatePillar(obs_pos[0],255);
    updatePillar(obs_pos[1],255);
    updatePillar(obs_pos[2],255);
    updatePillar(obs_pos[3],255);

    mu->updateEDTMap(0.4, 1.4, DevGeo::pos(pose_ptr->pose.position.x,
                                           pose_ptr->pose.position.y,
                                           pose_ptr->pose.position.z));

    // publish the data out
    msg.x_origin = dMap.getOrigin().x;
    msg.y_origin = dMap.getOrigin().y;
    msg.z_origin = dMap.getOrigin().z;
    memcpy(msg.payload8.data(),dMap.getMapPtr(),sizeof(float)*msg.x_length*msg.y_length*msg.z_length);
    pub.publish (msg);
}

void get_pose(const geometry_msgs::PoseStamped::ConstPtr &msg)
{
    got_pose = true;
    *pose_ptr = *msg;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "edt_node");
    ros::NodeHandle nh;

    // Normal initialization
    pose_ptr = boost::shared_ptr<geometry_msgs::PoseStamped>(new geometry_msgs::PoseStamped());
    got_pose = false;

    // Setup the map and map updater
    DevGeo::pos origin(-10,-10,-2);
    dMap.setMapSpecs(origin,0.2);
    pMap.setMapSpecs(origin,0.2);
    mu = new MapUpdater(&dMap, &pMap);

    // Setup the map publisher
    msg.x_length = dMap.getMaxX();
    msg.y_length = dMap.getMaxY();
    msg.z_length = 1;
    msg.x_origin = dMap.getOrigin().x;
    msg.y_origin = dMap.getOrigin().y;
    msg.z_origin = dMap.getOrigin().z;
    msg.res = dMap.getGridStep();
    msg.payload8.resize(sizeof(float)*msg.x_length*msg.y_length*msg.z_length);

    // Setup the timer
    ros::Timer mapTimer = nh.createTimer(ros::Duration(0.25),publishMap);

    // Setup the callbacks
    pub = nh.advertise<edt::CostMap> ("cost_map", 1);
    ros::Subscriber o_sub = nh.subscribe("/mavros/position/local", 1, get_pose);
    // Start the timer
    mapTimer.start();

    ros::spin();
    return 0;
}

