#include "helper_mapop.cuh"
#include "edt/LidarKernelWrapper.h"
#include <cooperative_groups.h>

namespace lidar
{
using namespace cuda_mapop;
//===
__device__ __forceinline__
void global2project(DevGeo::coord grid_crd,const ProjParams &mp, const DevMap &dev_map,
                    const LaserParams &lp, int &theta_idx, float &depth)
{
    DevGeo::pos vox_pos=coord2pos(grid_crd,dev_map.origin,dev_map.gridstep);
    float3 vox_pos_f3=make_float3(vox_pos.x,vox_pos.y,vox_pos.z);

    // from global frame to sensor frame
    cudaMat::SE3<float> camPos_inv=mp.camPoseInv;
    float3 camPoint=camPos_inv*vox_pos_f3;     // 3d pose in camera axis

    // calculate theta and theta_idx
    float theta = atan2(camPoint.y,camPoint.x);
    theta_idx=floor((theta - lp.theta_min)/lp.theta_inc +0.5);

    // get the depth
    if (fabs(camPoint.z)<0.1)
        depth=sqrt(camPoint.x*camPoint.x+camPoint.y*camPoint.y);
    else
        depth = -10;
}
//===
__global__
void lidarMapSingleUpdater(LaserParams lp,
                           ProjParams mp,
                           float *d_scan,
                           DevMap dev_map,
                           DevGeo::coord shift)
{
    DevGeo::coord s;
    s.z = blockIdx.x;
    s.y = threadIdx.x;

    float depth;
    int theta_idx;
    float lsr_depth;
    DevGeo::coord c;
    float val;

    for (s.x = 0; s.x < dev_map.updateRange.x; ++s.x)
    {
        c = s + shift;
//        // try to get the current value (can be ignored?)
//        if (!dev_map.getValue(&val,c))
//            continue;

        global2project(c,mp,dev_map,lp,theta_idx,depth);

        if (depth <= 0)
            continue;

        if (theta_idx<0 || theta_idx>=lp.scan_num)
            continue;

        lsr_depth=d_scan[theta_idx];

        if (isnan(lsr_depth) || lsr_depth <= 0.3)
          continue;

        if (depth < lsr_depth - 0.3)
        {
            updateOccupancy(c, 0, dev_map.DEV_pmap, dev_map.DEV_fmap, dev_map.mapRange);
        }
        else if (depth > lsr_depth + 0.3)
        {
            // not seen do nothing
        }
        else
        {
            updateOccupancy(c, 250, dev_map.DEV_pmap, dev_map.DEV_fmap, dev_map.mapRange);
        }
    }
}
//===
void lidarKernelWrapper(float *D_scan, DevMap *dev_map, ProjParams mp, LaserParams lp)
{
    const int gridSize = dev_map->updateRange.z;
    const int blkSize = dev_map->updateRange.y;

    // calculate the shift
    DevGeo::coord shift = dev_map->pos2coord(mp.center);
    shift.x -= dev_map->updateRange.x/2;
    shift.y -= dev_map->updateRange.y/2;
    shift.z -= dev_map->updateRange.z/2;

    lidarMapSingleUpdater<<<gridSize,blkSize>>>(lp,mp,D_scan,*dev_map,shift);
}
}

