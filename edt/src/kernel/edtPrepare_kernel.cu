#include <edt/EdtPrepareKernelWrapper.h>
namespace EdtPrepare
{
__global__
void edtPrepare(SeenDist *dev_buf_map, DevGeo::coord shift, int max_z, int min_z, DevMap dev_map,int w)
{
    DevGeo::coord c;
    c.x = blockIdx.x;
    c.y = threadIdx.x;
    unsigned char val = 0;
    bool seen = 0;
    bool occupied = false;
    bool observed = false;
    for (c.z=min_z;c.z<=max_z;c.z++)
    {
        if (dev_map.getValue(&val,&seen,c+shift))
        {
            if (val > 127)
                occupied = true;
            if (seen)
                observed = true;
        }
    }

    if (occupied)
        dev_buf_map[c.y*w+c.x].d = 0;
    else
        dev_buf_map[c.y*w+c.x].d = 1E20;

    // Assume (dev_map.bufRange.x/2,dev_map.bufRange.y/2)
    // is the current location of the drone!!
    int dx = c.x - dev_map.bufRange.x/2;
    int dy = c.y - dev_map.bufRange.y/2;
    if (observed || dx*dx+dy*dy<=4)
        dev_buf_map[c.y*w+c.x].s = true;
    else
        dev_buf_map[c.y*w+c.x].s = false;
}

void EdtPrepareKernelWrapper(DevMap *dev_map, const DevGeo::coord &shift,
                             int max_z, int min_z)
{
    const int gridSize = dev_map->bufRange.x;
    const int blkSize = dev_map->bufRange.y;
    edtPrepare<<<gridSize,blkSize>>>(dev_map->DEV_buf, shift,
                                     max_z, min_z, *dev_map,dev_map->bufRange.x);
}
}
