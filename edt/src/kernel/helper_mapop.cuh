#ifndef KERNEL_MAPOP_CUH
#define KERNEL_MAPOP_CUH

#include <stdio.h>
#include<iostream>
#include <cstring>
#include <cfloat>
#include <cassert>
#include "cuda_toolkit/cuda_geometry.h"

namespace cuda_mapop
{
//---
__device__ __forceinline__
DevGeo::coord pos2coord(const DevGeo::pos &p,const DevGeo::pos &origin, const float &gridstep)
{
    DevGeo::coord output;
    output.x = floor( (p.x - origin.x) / gridstep + 0.5);
    output.y = floor( (p.y - origin.y) / gridstep + 0.5);
    output.z = floor( (p.z - origin.z) / gridstep + 0.5);
    return output;
}
//---
__device__ __forceinline__
DevGeo::pos coord2pos(const DevGeo::coord & c,const DevGeo::pos &origin, const float &gridstep)
{
    DevGeo::pos output;
    output.x = c.x * gridstep + origin.x;
    output.y = c.y * gridstep + origin.y;
    output.z = c.z * gridstep + origin.z;

    return output;
}
//---
__device__ __forceinline__
bool updateOccupancy(const DevGeo::coord &c, const unsigned char occupancy,
                     unsigned char *D_pmap, bool *D_fmap, const int3 &mapRange)
{
    if (c.x<0 || c.x>=mapRange.x ||
            c.y<0 || c.y>=mapRange.y ||
            c.z<0 || c.z>=mapRange.z)
    {
        return false;
    }
    else
    {
        // BE VERY CAREFUL!!! As there is no write protection here
        int occu_idx=c.z*mapRange.x*mapRange.y+c.y*mapRange.x+c.x;
        D_pmap[occu_idx]=occupancy;
        D_fmap[occu_idx]=true;
        return true;
    }
}
}
#endif // KERNEL_CUH
