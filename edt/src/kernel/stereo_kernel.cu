#include "helper_mapop.cuh"
#include "edt/StereoKernelWrapper.h"
#include "edt/timer.h"
namespace stereo
{
using namespace cuda_mapop;
__device__ __forceinline__
void global2project(const DevGeo::coord &grid_crd,const ProjParams &mp, const DevMap &dev_map,
                    const StereoParams &sp, int2 &pix, float &depth)
{
    DevGeo::pos vox_pos=coord2pos(grid_crd,dev_map.origin,dev_map.gridstep);
    float3 vox_pos_f3=make_float3(vox_pos.x,vox_pos.y,vox_pos.z);

    // from global frame to sensor frame
    cudaMat::SE3<float> camPos_inv=mp.camPoseInv;
    float3 camPoint=camPos_inv*vox_pos_f3;     // 3d pose in camera axis

    // get the depth
    depth=camPoint.x;

    // calculate the pixel coordinate on the image
    pix.x=floor(-camPoint.y*sp.fx/depth+sp.cx+0.5);
    pix.y=floor(-camPoint.z*sp.fy/depth+sp.cy+0.5);
}
//===
__global__
void stereoMapSingleUpdater(StereoParams sp,
                            ProjParams mp,
                            float *d_depth,
                            DevMap dev_map,
                            DevGeo::coord shift)
{
    DevGeo::coord s;
    s.z = blockIdx.x;
    s.y = threadIdx.x;

    float depth;
    int2 pix;
    float img_depth;
    DevGeo::coord c;
    float val;

    for (s.x = 0; s.x < dev_map.updateRange.x; ++s.x)
    {
        c = s + shift;
//        // try to get the current value (can be ignored?)
//        if (!dev_map.getValue(&val,c))
//            continue;

        global2project(c,mp,dev_map,sp,pix,depth);

        if (depth <= 0 || depth >4.5)
            continue;

        if (pix.x < 0 || pix.x >= sp.cols || pix.y < 0 || pix.y >= sp.crop/*sp.rows*/)
          continue;

        img_depth=d_depth[sp.cols*pix.y+pix.x];

        if (isnan(img_depth) || img_depth <= 0.21)
          continue;

        if (depth < img_depth - 0.21)
        {
            updateOccupancy(c, 0, dev_map.DEV_pmap, dev_map.DEV_fmap, dev_map.mapRange);
        }
        else if (depth > img_depth + 0.21)
        {
            // not seen do nothing
        }
        else
        {
            updateOccupancy(c, 250, dev_map.DEV_pmap, dev_map.DEV_fmap, dev_map.mapRange);
        }
    }
}
//===
void stereoKernelWrapper(const StereoParams &sp,
                         const ProjParams &mp,
                         float *d_depth,
                         float* d_confi_map,
                         DevMap *dev_map)
{
    const int gridSize = dev_map->updateRange.z;
    const int blkSize = dev_map->updateRange.y;

    // calculate the shift
    DevGeo::coord shift = dev_map->pos2coord(mp.center);
    shift.x -= dev_map->updateRange.x/2;
    shift.y -= dev_map->updateRange.y/2;
    shift.z -= dev_map->updateRange.z/2;
    stereoMapSingleUpdater<<<gridSize,blkSize>>>(sp,mp,d_depth,*dev_map,shift);
}
}
