#include <ros/ros.h>
#include <edt/LinDistMap.h>
#include <edt/lidar.h>
#include <edt/stereo.h>
#include <sensor_msgs/PointCloud2.h>
#include <edt/CostMap.h>
#include <geometry_msgs/PoseStamped.h>
#include <gazebo_msgs/GetModelState.h>
#include <geometry_msgs/TransformStamped.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/CameraInfo.h>
#include <nav_msgs/Odometry.h>
#include <tf/tf.h>
#include <sensor_msgs/Imu.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#define SHOWPC

#ifdef SHOWPC
typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
PointCloud::Ptr pclOut (new PointCloud);
#endif
class EDTNODE
{
public:
    EDTNODE()
    {
        // load all the params
        
        // DO NOT READ FROM YAML, READ FROM CAMERA_INFO INSTEAD
        // nh.param<float>("/edt/cx", cx, 320);
        // nh.param<float>("/edt/cy", cy, 240);
        // nh.param<float>("/edt/fx", fx, 337.209);
        // nh.param<float>("/edt/fy", fy, 337.209);
        // nh.param<int>("/edt/depth_rows", depth_rows, 480);
        // nh.param<int>("/edt/depth_cols", depth_cols, 640);

        
        nh.param<int>("/edt/crop", crop, depth_rows);
        nh.param<float>("/edt/clear_range", clear_range, 10);
        nh.param<double>("/nndp_cpp/fly_height",FLYHEIGHT,1.0);

        dMap = new LinDistMap(100, 100);

        // publisher
    #ifdef SHOWPC
        pclOut->header.frame_id = "/map";
        pub_pc = nh.advertise<PointCloud> ("points3", 1);
    #endif
        pub_map = nh.advertise<edt::CostMap> ("cost_map", 1);

        // subscriber
        scan_sub = nh.subscribe("/scan", 1, &EDTNODE::get_scan, this);
        o_sub = nh.subscribe("/mavros/position/local", 1, &EDTNODE::get_pose, this);

        // subscribe to depth
        depth_sub = nh.subscribe("/revised_sensor/image",1,&EDTNODE::get_depth, this);
        ROS_INFO("Waiting for depth image camera info, for calibration params");
        auto msgptr = ros::topic::waitForMessage<sensor_msgs::CameraInfo>("/revised_sensor/camera_info",nh);

        cx = msgptr->K[2];
        cy = msgptr->K[5];
        fx = msgptr->K[0];
        fy = msgptr->K[4];

        depth_rows = msgptr->height;
        depth_cols = msgptr->width;

        ROS_INFO_STREAM("Received calibration: " << "cx=" << cx << " cy= " << cy << " fx=" << fx << " fy=" << fy << " width=" << depth_cols << " height=" << depth_rows);
        
        subConf = nh.subscribe("zed/confidence/confidence_map", 1, &EDTNODE::get_confi_map, this);

        // timer
        mapTimer = nh.createTimer(ros::Duration(0.1), &EDTNODE::publishMap, this);

        // Normal initialization
        laser_ptr = boost::shared_ptr<sensor_msgs::LaserScan>(new sensor_msgs::LaserScan());
        pose_ptr = boost::shared_ptr<geometry_msgs::PoseStamped>(new geometry_msgs::PoseStamped());
        depth_ptr=boost::shared_ptr<sensor_msgs::Image>(new sensor_msgs::Image());
        confidence_ptr=boost::shared_ptr<sensor_msgs::Image>(new sensor_msgs::Image());//0--100
        got_scan = false;
        got_pose = false;
        got_depth=false;
        got_confi_map=false;

        // Setup the map and map updater
        double gridSize = 0.2;
        DevGeo::pos origin(-100,-100,-0.2);
        dMap->setMapSpecs(origin,gridSize);
        int3 mapRange = make_int3(1000,1000,40);
        int3 bufRange = make_int3(dMap->getMaxX(),dMap->getMaxY(),1);
        int3 updateRange = make_int3(100,100,20);
        dev_map = new DevMap(origin,gridSize,mapRange,bufRange,updateRange);
        dev_map->createDeviceMaps();
        StereoParams sp(depth_rows,depth_cols,cx,cy,fx,fy,clear_range,crop);
        su=new StereoUpdater(dMap,dev_map,sp);
#ifdef SHOWPC
        pMap = new PbtyMap(mapRange.x, mapRange.y, mapRange.z);
        pMap->setMapSpecs(origin,gridSize);
#endif

        // Setup the map publisher
        cost_map_msg.x_length = dMap->getMaxX();
        cost_map_msg.y_length = dMap->getMaxY();
        cost_map_msg.z_length = 1;
        cost_map_msg.x_origin = dMap->getOrigin().x;
        cost_map_msg.y_origin = dMap->getOrigin().y;
        cost_map_msg.z_origin = dMap->getOrigin().z;
        cost_map_msg.res = dMap->getGridStep();
        cost_map_msg.payload8.resize(sizeof(SeenDist)*cost_map_msg.x_length*cost_map_msg.y_length*cost_map_msg.z_length);

        // Start the timer
        mapTimer.start();
    }
    //---
    ~EDTNODE()
    {
         dev_map->deleteDeviceMaps();
         delete dev_map;
         if (lu != NULL)
             delete lu;

         if (su != NULL)
             delete su;

         delete dMap;
#ifdef SHOWPC
         delete pMap;
#endif
    }
    //---
private:
    ros::NodeHandle nh;
    ros::Publisher pub_pc;
    ros::Subscriber scan_sub;
    ros::Subscriber o_sub;
    ros::Subscriber depth_sub;
    ros::Subscriber subConf;
    ros::Timer mapTimer;

    // Map updater
    LidarUpdater* lu = NULL;
    StereoUpdater* su= NULL;
    LinDistMap *dMap = NULL;
#ifdef SHOWPC
    PbtyMap *pMap = NULL;
#endif
    // Publisher
    ros::Publisher pub_map;
    edt::CostMap cost_map_msg;

    // Pointer to store the laser & pose
    boost::shared_ptr<sensor_msgs::LaserScan>  laser_ptr;
    boost::shared_ptr<geometry_msgs::PoseStamped>  pose_ptr;
    boost::shared_ptr<sensor_msgs::Image> depth_ptr, confidence_ptr;

    //int depth_rows=400,depth_cols=800;
    bool got_scan, got_pose, got_depth, got_confi_map;
    tf::Transform trans;

    //params
    float cx,cy,fx,fy;
    int depth_rows,depth_cols, crop;
    float clear_range;
    double FLYHEIGHT;

    // Device map
    DevMap *dev_map;
    //---
    void publishMap(const ros::TimerEvent&)
    {

        if (!got_pose)
            return;

        // Get the pose
        const clock_t begin_time = clock();
        tf::Quaternion quat;
        tf::quaternionMsgToTF(pose_ptr->pose.orientation, quat);
        trans.setRotation(quat);
        trans.setOrigin(tf::Vector3(pose_ptr->pose.position.x,
                                    pose_ptr->pose.position.y,
                                    pose_ptr->pose.position.z));

        // Update the probatility map
        if (got_depth)
        {
            su->makeStereoPt(trans,depth_ptr,confidence_ptr);
        }

        if (got_scan)
        {
            lu->makeLaserPt(trans, laser_ptr);
        }

        // Update the cost Map
        DevGeo::pos center(pose_ptr->pose.position.x,
                           pose_ptr->pose.position.y,
                           pose_ptr->pose.position.z);
        su->updateEDTMap(FLYHEIGHT-0.4, FLYHEIGHT+0.2, center);


        // Publish the distance map
        cost_map_msg.x_origin = dMap->getOrigin().x;
        cost_map_msg.y_origin = dMap->getOrigin().y;
        cost_map_msg.z_origin = dMap->getOrigin().z;
        memcpy(cost_map_msg.payload8.data(),dMap->getMapPtr(),sizeof(SeenDist)*cost_map_msg.x_length*cost_map_msg.y_length*cost_map_msg.z_length);
        pub_map.publish (cost_map_msg);
        std::cout <<"EDT node time: "<< float( clock () - begin_time ) /  CLOCKS_PER_SEC<<std::endl;

#ifdef SHOWPC
        CUDA_MEMCPY_D2H(pMap->getMapPtr(), dev_map->DEV_pmap, pMap->mapSizeInBytes());
        int rx = 200;
        int ry = 200;
        int rz = 30;
        DevGeo::coord c;
        DevGeo::coord shift = pMap->pos2coord(center);
        shift.x -= rx/2;
        shift.y -= ry/2;
        shift.z -= rz/2;
        DevGeo::pos p;
        for (c.x=0;c.x<rx;c.x++)
        {
            for (c.y=0;c.y<ry;c.y++)
            {
                for (c.z=0;c.z<rz;c.z++)
                {
                    unsigned char *val = pMap->pbty(c+shift);
                    if (val && *val >= 155)
                    {
                        p = pMap->coord2pos(c+shift);
                        pcl::PointXYZ clrP;
                        clrP.x = p.x;
                        clrP.y = p.y;
                        clrP.z = p.z;
                        pclOut->points.push_back (clrP);
                    }
                }
            }
        }
        pcl_conversions::toPCL(ros::Time::now(), pclOut->header.stamp);
        pub_pc.publish (pclOut);
        pclOut->clear();
#endif
    }
    //---
    void get_scan(const sensor_msgs::LaserScan::ConstPtr &msg)
    {
        // create the laser updater from the scan message
        if (!got_scan)
        {
            lu = new LidarUpdater(dMap,dev_map,msg);
        }
        got_scan = true;
        *laser_ptr = *msg;
    }
    //---
    void get_pose(const geometry_msgs::PoseStamped::ConstPtr &msg)
    {
        got_pose = true;
        *pose_ptr = *msg;
    }
    //---
    void get_confi_map(const sensor_msgs::Image::ConstPtr& msg)
    {
        got_confi_map=true;
        *confidence_ptr=*msg;
    }
    //---
    void get_depth(const sensor_msgs::Image::ConstPtr& msg)
    {
        if(msg->width!=depth_cols||msg->height!=depth_rows)
        {

            su->reAllocMem(msg->height,msg->width);
            depth_cols=msg->width;
            depth_rows=msg->height;
        }
        got_depth=true;
        *depth_ptr=*msg;
    }
};
//---
int main(int argc, char **argv)
{
    ros::init(argc, argv, "edt_node");
    EDTNODE edt;
    ros::spin();

    return 0;
}
