#include <iostream>
#include <chrono>  // for high_resolution_clock
#include <FixedMap.h>
#include <fstream>
#include <MapUpdater.h>
using namespace std;

float RandomFloat(float a, float b) {
    float random = ((float) rand()) / (float) RAND_MAX;
    float diff = b - a;
    float r = random * diff;
    return a + r;
}

int main()
{
    ofstream myfile;
    myfile.open("map.txt");
    FixedMap testMap(100,100,20);
    DevGeo::pos origin;
    origin.x = -10;
    origin.y = -10;
    origin.z = -2;
    testMap.setMapSpecs(origin,0.2);
    MapUpdater mu(&testMap);
    srand(1);

    DevGeo::pos center;
    DevGeo::pos tmp;
    std::vector<DevGeo::pos> points;
//---
    for (int z=-240;z<240;z++)
    {
        tmp.z = z/120.0;

        for (int x=-320;x<320;x++)
        {
            tmp.y = 0.5*sin(x/36.0)+2;
            tmp.x = x/36.0;
            points.push_back(tmp);
        }
    }


    auto start = std::chrono::high_resolution_clock::now();


    mu.updateRay(center,points);


    testMap.updateDistanceMap();

    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    std::cout << "Elapsed time: " << elapsed.count() << " s\n";
    points.clear();
//---


//---
    for (int z=-240;z<240;z++)
    {
        tmp.z = z/120.0;

        for (int x=-320;x<320;x++)
        {
            tmp.y = 0.5*sin(x/36.0)+5;
            tmp.x = x/36.0;
            points.push_back(tmp);
        }
    }


    start = std::chrono::high_resolution_clock::now();
    mu.updateRay(center,points);
    testMap.updateDistanceMap();
    finish = std::chrono::high_resolution_clock::now();
    elapsed = finish - start;
    std::cout << "Elapsed time: " << elapsed.count() << " s\n";
    points.clear();
//---


    //===================================
    DevGeo::coord c;
    for (int x=0;x<100;x++)
    {
        for (int y=0;y<100;y++)
        {
            for (int z=0;z<20;z++)
            {
                c.x=x;
                c.y=y;
                c.z=z;
                myfile<<sqrt(testMap.data(c)->dist_sq)<<" ";
            }
        }
    }
    myfile<<"\n";
    myfile.close();
    std::cout<<"Finish"<<std::endl;

    return 0;
}
