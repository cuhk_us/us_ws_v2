TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    BucketQueue.cpp \
    EDTMapBase.cpp \
    FixedMap.cpp \
    MapUpdater.cpp

HEADERS += \
    ../include/edt/BucketQueue.h \
    ../include/edt/EDTMapUtilities.h \
    ../include/edt/EDTMapBase.h \
    ../include/edt/FixedMap.h \
    ../include/edt/MapUpdater.h

INCLUDEPATH += \
    ../include
