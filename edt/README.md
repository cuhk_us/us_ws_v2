# edt_cuda learning notes
[udacity website](https://classroom.udacity.com/courses/cs344/lessons/55120467/concepts/670611900923)

## communication pattern
- map  
- gather
- scatter
- stencil
- transpose

[![image.png](https://i.postimg.cc/L82r5zQL/image.png)](https://postimg.cc/dLNWSTYV)

## memery model
[![image.png](https://i.postimg.cc/3rVP2k9f/image.png)](https://postimg.cc/qgcbTMP2)


## syncronize


```
__shared__ int arry[128];
array[idx]=threadIdx;
// wait for every read op to compelete
__syncthreads();
int temp=array[idx+1];
// write from shared mem to local mem
__syncthreads();
array[idx]=temp;
__syncthreads();
```

## atomic opp
is equivlent of adding variables in shared mem

```

g[i]=g[i]+1    ------>  atomicAdd(&g[i],1);

```

# thrust
- vector

```
thrust::device_vector<int> dv<100>;
thrust::host_vector<int> hv<100,25>;
// cudaMemcopy() in the background
dv=hv;
```
- Note: device_vector.push_back() cannot be used in kernel function
