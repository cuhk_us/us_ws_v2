#ifndef CUDA_PBTYMAP_H
#define CUDA_PBTYMAP_H
#include <cuda_toolkit/cuda_geometry.h>
#include <edt/LinDistMap.h>

class DevMap
{
public:
    DevMap(DevGeo::pos origin_, float gridstep_, int3 mapRange_, int3 bufRange_, int3 updateRange_):
        origin(origin_),
        mapRange(mapRange_),
        gridstep(gridstep_),
        bufRange(bufRange_),
        updateRange(updateRange_)
    {
        // allocate and initialize the pmap and fmap
        p_mapByteSize = mapRange.x*mapRange.y*mapRange.z*sizeof(unsigned char);
        f_mapByteSize = mapRange.x*mapRange.y*mapRange.z*sizeof(bool);
        bufByteSize = bufRange.x*bufRange.y*bufRange.z*sizeof(SeenDist);
    }
    //---
    ~DevMap()
    {

    }
    //---
    void createDeviceMaps()
    {
        CUDA_ALLOC_DEV_MEM(&DEV_pmap,p_mapByteSize);
        CUDA_ALLOC_DEV_MEM(&DEV_fmap,f_mapByteSize);
        CUDA_ALLOC_DEV_MEM(&DEV_buf,bufByteSize);
        CUDA_DEV_MEMSET(DEV_pmap,0,p_mapByteSize);
        CUDA_DEV_MEMSET(DEV_fmap,0,f_mapByteSize);
        CUDA_DEV_MEMSET(DEV_buf,0,bufByteSize);
    }
    //---
    void deleteDeviceMaps()
    {
        CUDA_FREE_DEV_MEM(DEV_pmap);
        CUDA_FREE_DEV_MEM(DEV_fmap);
        CUDA_FREE_DEV_MEM(DEV_buf);
    }
    //---
    DevGeo::coord pos2coord(const DevGeo::pos & p) const
    {
        DevGeo::coord output;
        output.x = floor( (p.x - origin.x) / gridstep + 0.5);
        output.y = floor( (p.y - origin.y) / gridstep + 0.5);
        output.z = floor( (p.z - origin.z) / gridstep + 0.5);
        return output;
    }
    //---
    DevGeo::pos coord2pos(const DevGeo::coord & c) const
    {
        DevGeo::pos output;
        output.x = c.x * gridstep + origin.x;
        output.y = c.y * gridstep + origin.y;
        output.z = c.z * gridstep + origin.z;

        return output;
    }
    //---
    __device__
    bool getValue(unsigned char *val, bool* seen, const DevGeo::coord & s)
    {
        if (s.x<0 || s.x>=mapRange.x ||
                s.y<0 || s.y>=mapRange.y ||
                s.z<0 || s.z>=mapRange.z)
        {
            *val = 250;
            *seen = false;
            return 0;
        }
        else
        {
            int id = s.z*mapRange.x*mapRange.y+s.y*mapRange.x+s.x;
            *val = DEV_pmap[id];
            *seen = DEV_fmap[id];
            return 1;
        }
    }
public:
    unsigned char *DEV_pmap;
    bool *DEV_fmap;
    DevGeo::pos origin;
    int3 mapRange;
    int3 updateRange;
    float gridstep;
    int p_mapByteSize;
    int f_mapByteSize;
    //--- for the distance map device buffer
    SeenDist *DEV_buf;
    int bufByteSize;
    int3 bufRange;
};
#endif // CUDA_PBTYMAP_H
