#ifndef CUDA_GEOMETRY_CUH
#define CUDA_GEOMETRY_CUH
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <math.h>
#include <stdio.h>
#include <iostream>
#include <cstring>
#include <cfloat>
#include <cassert>
#include "cuda_toolkit/se3.cuh"

namespace DevGeo
{
struct coord
{
    int x,y,z;
    //---
    __device__  __host__
    coord():x(0),y(0),z(0)
    {

    }
    //---
    __device__  __host__
    coord(int a,int b, int c)
    {
        x=a;
        y=b;
        z=c;
    }
    //---
    __device__ __host__
    int square()
    {
        return x*x+y*y+z*z;
    }
    //---
    __device__  __host__
    coord operator+(coord const& rhs) const
    {
        coord tmp;
        tmp.x = x + rhs.x;
        tmp.y = y + rhs.y;
        tmp.z = z + rhs.z;
        return tmp;
    }
    //---
    __device__  __host__
    coord operator-(coord const& rhs) const
    {
        coord tmp;
        tmp.x = x - rhs.x;
        tmp.y = y - rhs.y;
        tmp.z = z - rhs.z;
        return tmp;
    }
    //---
    __device__  __host__
    coord& operator=(coord const& rhs)
    {
        x = rhs.x;
        y = rhs.y;
        z = rhs.z;
        return *this;
    }
    //---
    __device__  __host__
    bool operator==(coord const& rhs) const
    {
        if(x==rhs.x && y==rhs.y && z==rhs.z)
            return true;
        else
            return false;
    }
    //---
    __device__  __host__
    bool operator<(coord const& rhs) const
    {
        if(x < rhs.x)
        {
            return true;
        }
        else if (x == rhs.x)
        {
            if (y < rhs.y)
            {
                return true;
            }
            else if (y == rhs.y)
            {
                if (z < rhs.z)
                {
                    return true;
                }
            }
        }
        return false;
    }
    //---
    __device__  __host__
    int& at(int i)
    {
        switch (i) {
        case 0:
            return x;
        case 1:
            return y;
        case 2:
            return z;
        default:
            assert(false);
            return x;
        }
    }
    void printC(char name[] )
    {
        printf("%s :(%d , %d, %d) \n",name,x,y,z);
    }
};
//===
struct pos
{
    float x,y,z;
    //---
    __device__ __host__
    pos():x(0.0),y(0.0),z(0.0)
    {

    }
    //---
    __device__ __host__
    pos(float x_, float y_, float z_):x(x_),y(y_),z(z_)
    {

    }
    //---
    __device__ __host__
    pos operator+(pos const& rhs) const
    {
        pos tmp;
        tmp.x = x + rhs.x;
        tmp.y = y + rhs.y;
        tmp.z = z + rhs.z;
        return tmp;
    }
    //---
    __device__ __host__
    pos operator-(pos const& rhs) const
    {
        pos tmp;
        tmp.x = x - rhs.x;
        tmp.y = y - rhs.y;
        tmp.z = z - rhs.z;
        return tmp;
    }
    //---
    __device__ __host__
    pos operator/(float const& rhs) const
    {
        pos tmp;
        tmp.x = x / rhs;
        tmp.y = y / rhs;
        tmp.z = z / rhs;
        return tmp;
    }
    //---
    __device__ __host__
    pos operator*(float const& rhs) const
    {
        pos tmp;
        tmp.x = x * rhs;
        tmp.y = y * rhs;
        tmp.z = z * rhs;
        return tmp;
    }
    //---
    __device__ __host__
    pos& operator=(pos const& rhs)
    {
        x = rhs.x;
        y = rhs.y;
        z = rhs.z;
        return *this;
    }
    //---
    __device__ __host__
    bool operator==(pos const& rhs) const
    {
        if (x==rhs.x && y==rhs.y && z==rhs.z)
            return true;
        else
            return false;
    }
    //---
    __device__ __host__
    float square()
    {
        return x*x+y*y+z*z;
    }
    //---
    __device__ __host__
    float& at(int i)
    {
        switch (i) {
        case 0:
            return x;
        case 1:
            return y;
        case 2:
            return z;
        default:
            assert(false);
            return x;
        }
    }
    //---
    __device__ __host__
    const float& const_at(int i) const
    {
        switch (i) {
        case 0:
            return x;
        case 1:
            return y;
        case 2:
            return z;
        default:
            assert(false);
            return x;
        }
    }
    //---
    void printP(char name[] )
    {
        printf("%s :(%f , %f, %f)\n ",name,x,y,z);
    }
};
}
//===
struct ProjParams
{
    cudaMat::SE3<float> camPos;
    cudaMat::SE3<float> camPoseInv;
    DevGeo::pos center;
};
//===
struct StereoParams
{
    int rows, cols, crop;
    float cx, cy, fx, fy, range;
    StereoParams(int rows_, int cols_,
                 float cx_,float cy_,
                 float fx_, float fy_,
                 float range_, int crop_):
        rows(rows_),cols(cols_),
        cx(cx_),cy(cy_),
        fx(fx_),fy(fy_),
        range(range_), crop(crop_)
    {}
};
//===
struct LaserParams
{
    float max_r, theta_inc, theta_min;
    int scan_num;
    LaserParams(int scan_num_,
                float max_r_, float theta_inc_,
                float theta_min_):
        scan_num(scan_num_),
        max_r(max_r_),
        theta_inc(theta_inc_),
        theta_min(theta_min_)
    {}
    LaserParams()
    {}
};
//===
static void hdlCudaErr(cudaError err, const char* const func, const char* const file, const int line)
{
    if (err != cudaSuccess)
    {
        std::cerr << "CUDA error at: " << file << ":" << line << std::endl;
        std::cerr << cudaGetErrorString(err) << " " << func << std::endl;
        exit(1);
    }
}
#define CUDA_ALLOC_DEV_MEM(devPtr,size) hdlCudaErr(cudaMalloc(devPtr,size), __FUNCTION__, __FILE__, __LINE__)
#define CUDA_MEMCPY_H2D(dst,src,count) hdlCudaErr(cudaMemcpy(dst,src,count,cudaMemcpyHostToDevice), __FUNCTION__, __FILE__, __LINE__)
#define CUDA_MEMCPY_D2H(dst,src,count) hdlCudaErr(cudaMemcpy(dst,src,count,cudaMemcpyDeviceToHost), __FUNCTION__, __FILE__, __LINE__)
#define CUDA_FREE_DEV_MEM(devPtr) hdlCudaErr(cudaFree(devPtr), __FUNCTION__, __FILE__, __LINE__)
#define CUDA_DEV_MEMSET(devPtr,value,count) hdlCudaErr(cudaMemset(devPtr,value,count), __FUNCTION__, __FILE__, __LINE__)
#endif // CUDA_GEOMETRY_CUH
