#ifndef PBTYMAP_H
#define PBTYMAP_H
#include <edt/EDTMapUtilities.h>
#include <edt/MapBase.h>

class PbtyMap : public MapBase
{
public:
    PbtyMap(int maxX, int maxY, int maxZ);
    unsigned char* pbty(const DevGeo::coord &s);
    int getMaxX()
    {
      return _maxX;
    }
    int getMaxY()
    {
      return _maxY;
    }
    int getMaxZ()
    {
    return _maxZ;
    }
    unsigned char *getMapPtr()
    {
      return _map;
    }
    int mapSizeInBytes()
    {
        return _mapByteSize;
    }
public:
    virtual ~PbtyMap();
private:
    int _maxX,_maxY,_maxZ;
    unsigned char *_map;
    int _mapByteSize;
};

#endif // PBTYMAP_H
