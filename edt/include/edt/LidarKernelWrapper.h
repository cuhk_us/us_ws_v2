#ifndef LIDARKERNELWRAPPER_H
#define LIDARKERNELWRAPPER_H
#include <cuda_toolkit/cuda_map.h>
namespace lidar
{
void lidarKernelWrapper(float *D_scan, DevMap *dev_map, ProjParams mp, LaserParams lp);
}
#endif // LIDARKERNELWRAPPER_H
