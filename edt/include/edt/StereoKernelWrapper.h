#ifndef STEREOKERNELWRAPPER_H
#define STEREOKERNELWRAPPER_H
#include <cuda_toolkit/cuda_map.h>
namespace stereo {


void stereoKernelWrapper(const StereoParams &sp,
                  const ProjParams &mp,
                  float *d_depth,
                  float* d_confi_map,
                  DevMap *dev_map);
}
#endif // STEREOKERNELWRAPPER_H
