#ifndef LIDAR_H
#define LIDAR_H

#include <stdio.h>
#include <iostream>
#include <tf/tf.h>
#include <ros/ros.h>
#include <edt/PbtyMap.h>
#include <edt/MapUpdater.h>
#include <sensor_msgs/LaserScan.h>
#include "cuda_toolkit/se3.cuh"
#include "cuda_toolkit/cuda_geometry.h"

class LidarUpdater : public MapUpdater
{
public:
  LidarUpdater(LinDistMap *dmap, DevMap *dev_map, const sensor_msgs::LaserScan::ConstPtr &msg);
  ~LidarUpdater();
  void makeLaserPt(const tf::Transform &trans,const sensor_msgs::LaserScan::ConstPtr &scan);

private:
  void copyScan(const sensor_msgs::LaserScan::ConstPtr &scan);

public:
  int _scan_sz;
  float *_D_scan ;
  LaserParams _lp;
};

#endif // LIDAR_H
