#ifndef STEREO_H
#define STEREO_H

#include <tf/tf.h>
#include <sensor_msgs/Image.h>
#include <ros/ros.h>
#include "std_msgs/Float32.h"
#include "edt/StereoKernelWrapper.h"
#include "edt/MapUpdater.h"

class StereoUpdater : public MapUpdater
{
public:

    //constructor
    StereoUpdater(LinDistMap *dmap, DevMap *dev_map, StereoParams p);

    //deconstructor
    ~StereoUpdater();

    //make the pbty map based on stereo input
    void makeStereoPt(const tf::Transform &trans, const sensor_msgs::Image::ConstPtr &depthPoint,
                      const sensor_msgs::Image::ConstPtr &confidence_ptr);

    void reAllocMem(int newRows, int newCols);

private:
    void topic2Dmem(const sensor_msgs::Image::ConstPtr &depthPoint,
                    const sensor_msgs::Image::ConstPtr &confidence_ptr);

    // memory management
    void allocMem(int rows,int cols);
    void freeMem();


private:
    StereoParams _sp;

    float *_D_depth,*_D_confi_map;
    int _depth_size,_confi_map_size;
};

#endif // STEREO_H
