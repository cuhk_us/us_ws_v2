#ifndef EDTMAPUTILITIES_H
#define EDTMAPUTILITIES_H
#include <limits>
#include <cmath>
#include <list>
#include <assert.h>
//#include <unordered_set>
#include <iostream>
#define USE2DEDT
namespace EDTMap
{

static const int MAX_RANGE = 10; // the maximum distance measured in grid number
static const int MAX_RANGE_SQ = MAX_RANGE * MAX_RANGE;
static const int BUCKET_SIZE = MAX_RANGE_SQ; // the bucket queue bucket size
static const int NB_RANGE = 1; // the range of neighbours, 1 means 26 neighbours in 3D
static const int NB_SIZE = (NB_RANGE*2+1)*(NB_RANGE*2+1)*(NB_RANGE*2+1)-1; // the range of neighbours, 1 means 26 neighbours in 3D
static const short int NOT_IN_Q = -1;
static const unsigned short OCCUPY_TH = 127;

}
#endif // EDTMAPUTILITIES_H
