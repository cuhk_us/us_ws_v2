#ifndef MAPBASE_H
#define MAPBASE_H
#include <edt/EDTMapUtilities.h>
#include <list>
#include <cuda_toolkit/cuda_geometry.h>
class MapBase
{
public:
    MapBase();
    void setMapSpecs(const DevGeo::pos & origin, const double & gridstep);
    void setOrigin(const DevGeo::pos & origin);
    DevGeo::pos getOrigin() const;
    double getGridStep() const;
    DevGeo::coord pos2coord(const DevGeo::pos & p) const;
    DevGeo::pos coord2pos(const DevGeo::coord & c) const;

public:
    virtual ~MapBase();

protected:
    DevGeo::pos _origin;
    double _gridstep;
};

#endif // MAPBASE_H
