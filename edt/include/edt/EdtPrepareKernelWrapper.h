#ifndef EDTPREPAREKERNELWRAPPER_H
#define EDTPREPAREKERNELWRAPPER_H
#include <cuda_toolkit/cuda_map.h>
#include <edt/LinDistMap.h>
namespace EdtPrepare {

void EdtPrepareKernelWrapper(DevMap *dev_map, const DevGeo::coord &shift,
                             int max_z, int min_z);
}
#endif // EDTPREPAREKERNELWRAPPER_H
