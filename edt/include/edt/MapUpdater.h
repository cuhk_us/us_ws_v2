#ifndef MAPUPDATER_H
#define MAPUPDATER_H
#include <edt/LinDistMap.h>
#include <edt/PbtyMap.h>
#include <cuda_toolkit/cuda_map.h>
#include <tf/tf.h>
//#include <pcl_ros/point_cloud.h>
//#include <pcl/point_types.h>
//#include <ros/ros.h>

class MapUpdater
{
public:
    MapUpdater(LinDistMap *dmap, DevMap *dev_map);
    void updateEDTMap(const double &min_z_pos, const double &max_z_pos, const DevGeo::pos &center);

private:
    cudaMat::SE3<float> getCamPos(const tf::Transform &trans) const;

protected:
    void updateProjection(const tf::Transform &trans);

protected:
    LinDistMap* _d_map;
    DevMap *_dev_map;
    ProjParams _mp;
};

#endif // MAPUPDATER_H
