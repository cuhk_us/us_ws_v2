#ifndef LINDISTMAP_H
#define LINDISTMAP_H
#include <edt/EDTMapUtilities.h>
#include <edt/MapBase.h>
#include <edt/PbtyMap.h>

// !!! Note:
// This is limited to be a 2D map, because 3D version on CPU cannot
// achieve real time.
// !!!
struct SeenDist
{
    float d;
    bool s;
};

class LinDistMap : public MapBase
{
public:
    LinDistMap(int maxX, int maxY);
    float distAt(const DevGeo::coord & s, const float default_value) const;
    bool isSeen(const DevGeo::coord & s, const bool default_value) const;
    void edt();
    void updateFromPbtyMap(PbtyMap *pMap, int minZ, int maxZ, const DevGeo::coord &shift);
    SeenDist* getMapPtr() {return _map;}
    int getMaxX() {return _w;}
    int getMaxY() {return _h;}
    int square(const int &x) { return x*x; }
    void copyData(const unsigned char* data, int maxX, int maxY,
                            const DevGeo::pos &origin, const double &gridstep);
    SeenDist minDistOnLineSeg(const DevGeo::pos & p0, const DevGeo::pos & p1) const;
    void evaTrajectory(const std::list<DevGeo::pos> &posList, const double &rClose,
                        const double &rVehicle, double &cost, bool &collision, const double &curr_yaw) const;

public:
    virtual ~LinDistMap();

private:
    SeenDist *_map;
    float INF = 1E20;
    float *_d_h, *_d_w;
    int *_v_h, *_v_w;
    float *_z_h, *_z_w;
    int _w, _h;
    float *_f;
private:
    float *dt(float *f, int n);
};

#endif // LINDISTMAP_H
